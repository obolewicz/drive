//
//  PDFCreator.swift
//  PDF
//
//  Created by Marcin Obolewicz on 04.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import UIKit

protocol HtmlReportCreatorProtocol {
    func createHtmlReport() -> String
}

class PDFCreator: NSObject {
    var htmlCreator: HtmlReportCreatorProtocol
    
    init(htmlCreator: HtmlReportCreatorProtocol) {
        self.htmlCreator = htmlCreator
        super.init()
    }
    
    func createPDF(fileName: String) {
        let html = self.htmlCreator.createHtmlReport()
        let fmt = UIMarkupTextPrintFormatter(markupText: html)
        let render = UIPrintPageRenderer()
        render.addPrintFormatter(fmt, startingAtPageAt: 0)
        
        let page = CGRect(x: 0, y: 0, width: 595.2, height: 841.8) // A4, 72 dpi
        let printable = page.insetBy(dx: 0, dy: 0)
        
        render.setValue(NSValue(cgRect: page), forKey: "paperRect")
        render.setValue(NSValue(cgRect: printable), forKey: "printableRect")
        
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, page, nil)
        print(render.numberOfPages)
        if render.numberOfPages > 0 {
            for i in 1...render.numberOfPages {
                
                UIGraphicsBeginPDFPage();
                let bounds = UIGraphicsGetPDFContextBounds()
                render.drawPage(at: i - 1, in: bounds)
            }
        }
        UIGraphicsEndPDFContext()
        pdfData.write(toFile: self.docunmetsPath(fileName: fileName), atomically: true)
    }
    
    func docunmetsPath(fileName: String) -> String{
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        return "\(documentsPath)/\(fileName).pdf"
    }
    
}
