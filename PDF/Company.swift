//
//  Company.swift
//  PDF
//
//  Created by Marcin Obolewicz on 04.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import Foundation
import RealmSwift

class Company: Object {
    public dynamic var companyId: Int = 0
    public dynamic var name: String = ""
    public dynamic var nip: String = ""
    public dynamic var street: String = ""
    public dynamic var city: String = ""
    public dynamic var zipCode: String = ""
    
    public let vehicles = List<Vehicle>()
    public let drivers = List<Driver>()
    public let reports = List<Report>()
    
    override class func primaryKey() -> String? {
        return "companyId"
    }
    
    private func incrementID() -> Int {
        return self.incrementID(idProperty: "companyId", class: Company.self)
    }
    
    convenience public required init(autoincrementId: Bool) {
        self.init()
        if autoincrementId {
            self.companyId = incrementID()
        }
    }
    
    public func fullAddress() -> String {
        return self.street + " " + self.zipCode + " " + self.city
    }
}
