//
//  SettingsViewModel.swift
//  PDF
//
//  Created by Marcin Obolewicz on 18.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import UIKit
import RealmSwift
import ReactiveCocoa
import ReactiveSwift

protocol SettingsViewModelProtocol {
    var driverName: String { get set }
    var driverSurname: String { get set }
    var driverEmail: String { get set }
    
    var currentVehicle: DynamicProperty<Vehicle> { get }
    
    var companyName: String { get set }
    var companyNIP: String { get set }
    var companyStreet: String { get set }
    var companyCity: String { get set }
    var companyZipCode: String { get set }
}

class SettingsViewModel: SettingsViewModelProtocol {
    dynamic public var driverName: String = "" {
        didSet {
            Realm.update {
               DriverDataManager.shared.currentDriver.firstName = driverName
            }
        }
    }
    dynamic public var driverSurname: String = "" {
        didSet {
            Realm.update {
                DriverDataManager.shared.currentDriver.lastName = driverSurname
            }
        }
    }
    dynamic public var driverEmail: String = "" {
        didSet {
            Realm.update {
                DriverDataManager.shared.currentDriver.email = driverEmail
            }
        }
    }
    internal var currentVehicle: DynamicProperty<Vehicle>
    dynamic public var companyName: String = "" {
        didSet {
            Realm.update {
                DriverDataManager.shared.currentCompany.name = companyName
            }
        }
    }
    dynamic public var companyNIP: String = "" {
        didSet {
            Realm.update {
                DriverDataManager.shared.currentCompany.nip = companyNIP
            }
        }
    }
    dynamic public var companyStreet: String = "" {
        didSet {
            Realm.update {
                DriverDataManager.shared.currentCompany.street = companyStreet
            }
        }
    }
    dynamic public var companyCity: String = "" {
        didSet {
            Realm.update {
                DriverDataManager.shared.currentCompany.city = companyCity
            }
        }
    }
    dynamic public var companyZipCode: String = "" {
        didSet {
            Realm.update {
                DriverDataManager.shared.currentCompany.zipCode = companyZipCode
            }
        }
    }
    
    init(){
        self.driverName = DriverDataManager.shared.currentDriver.firstName
        self.driverSurname = DriverDataManager.shared.currentDriver.lastName
        self.driverEmail = DriverDataManager.shared.currentDriver.email
        
        self.currentVehicle = DynamicProperty<Vehicle>(object: DriverDataManager.shared, keyPath: "currentVehicle")
        
        self.companyName = DriverDataManager.shared.currentCompany.name
        self.companyNIP = DriverDataManager.shared.currentCompany.nip
        self.companyStreet = DriverDataManager.shared.currentCompany.street
        self.companyCity = DriverDataManager.shared.currentCompany.city
        self.companyZipCode = DriverDataManager.shared.currentCompany.zipCode
    }
}
