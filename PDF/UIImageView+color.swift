//
//  UIImageView+color.swift
//  PDF
//
//  Created by Marcin Obolewicz on 11.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import UIKit

extension UIImageView {
    public func setImageColor(color: UIColor) {
        if self.image != nil {
            self.image = self.image!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            self.tintColor = color
        }
    }
}
