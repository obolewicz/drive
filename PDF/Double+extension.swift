//
//  Double+extension.swift
//  PDF
//
//  Created by Marcin Obolewicz on 12.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import Foundation

extension Double {
    public mutating func percent(ofValue: Double) -> Int{
        if ofValue == 0{
            return 0
        }
        
        return Int(Darwin.round((self / ofValue * Double(100))))
    }
    
    public func durationString() -> String{
        return Int(self).durationString()
    }
    
    func distanceFormatted() -> String {
        return String(format: "%1.2f km", (self / 1000.0))
    }
    
    func priceFormatted() -> String {
        return String(format: "%1.3f pln", self)
    }

}
