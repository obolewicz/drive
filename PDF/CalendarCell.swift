//
//  CalendarCell.swift
//  DriveMate
//
//  Created by Marcin Obolewicz on 06.01.2017.
//  Copyright © 2017 Luxmed. All rights reserved.
//

import UIKit
import JTAppleCalendar

class CalendarCell: JTAppleDayCellView {
    @IBOutlet weak var selectedView: UIView!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var dotView: UIView!
    public var numberOfItems = 0
    
    class public func identifier() -> String {
        return "CalendarCell"
    }
    
    public func update(date: Date, viewModel: HistoryViewModelProtocol) {
        self.numberOfItems = viewModel.itemsCountInDay(date)
        self.dayLabel.textColor = numberOfItems == 0 ? UIColor.black : UIColor.white
        self.dotView.backgroundColor = numberOfItems == 0 ? UIColor.white : UIColor.customPurple()
    }
}
