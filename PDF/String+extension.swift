//
//  String+extension.swift
//  PDF
//
//  Created by Marcin on 31.012.2016.
//

import UIKit

extension String {
    public func durationInt() -> Int {
        let components = self.components(separatedBy: ":")
        if components.count == 3 {
            guard let hours = Int(components[0]) else { return 0}
            guard let minutes = Int(components[1]) else { return 0}
            guard let seconds = Int(components[2]) else { return 0}
            return (hours * 60 * 60) + (minutes * 60) + seconds
        }
        else if components.count == 2{
            guard let minutes = Int(components[0]) else { return 0}
            guard let seconds = Int(components[1]) else { return 0}
            return (minutes * 60) + seconds
        }
        return 0
    }
    
    public func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    public func uppercaseFirst() -> String {
        return String(self.characters.prefix(1)).uppercased() + String(characters.dropFirst())
    }
    
    public func docunmetPath() -> String{
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        return "\(documentsPath)/\(self).pdf"
    }
}
