//
//  ReportPreparationViewController.swift
//  PDF
//
//  Created by Marcin Obolewicz on 17.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import UIKit
import Eureka

class ReportDetailsViewController: FormViewController {
    var viewModel: ReportDetailsViewModelProtocol = ReportDetailsViewModel()
    
    let datesSection = Section("Report Period".localized())
    let chooseRidesSection = Section("Choose Rides".localized())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Report Details".localized()
        
        self.setupForm()
        self.setupNavigationRightButton()
        self.bindData()
    }
    
    func bindData() {
        self.viewModel.viewEditionDisabled.signal.observeValues { (value: Bool) in
            for row in self.form.rows {
                row.disabled = Condition(booleanLiteral: value)
                row.evaluateDisabled()
            }
        }
    }
    
    func setupNavigationRightButton() {
        if viewModel.viewEditionDisabled.value == true {
            self.setupRightButtonsReadyReport()
        }
        else {
            self.setupRightButtonGenerate()
        }
    }
    
    func setupRightButtonsReadyReport() {
        let editReportButton = UIBarButtonItem(title: "Edit".localized(), style: .plain, target: self, action: #selector(editReportButtonAction))
        let showPDFButton = UIBarButtonItem(title: "PDF".localized(), style: .plain, target: self, action: #selector(showPDFAction))
        self.navigationItem.rightBarButtonItems = [editReportButton, showPDFButton]
    }
    
    func setupRightButtonGenerate() {
        let generatePDFButton = UIBarButtonItem(title: "Generate".localized(), style: .plain, target: self, action: #selector(generateReportButtonAction))
        self.navigationItem.rightBarButtonItems = [generatePDFButton]
    }
    
    func showPDFAction() {
        self.performSegue(withIdentifier: "showGenerateRaport", sender: self)
    }
    
    func editReportButtonAction() {
        self.updateRidesList()
        self.updateButtonState()
    }
    
    func generateReportButtonAction() {
        self.performSegue(withIdentifier: "showGenerateRaport", sender: self)
        self.viewModel.createReport()
        self.updateButtonState()
    }
    
    private func updateButtonState() {
        self.viewModel.viewEditionDisabled.value = !viewModel.viewEditionDisabled.value
        self.setupNavigationRightButton()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if segue.identifier == "showGenerateRaport" {
            if let viewController = segue.destination as? GenerateReportViewController {
                viewController.viewModel = self.viewModel
            }
        }
    }
    
    private func setupForm() {
        self.form = self.datesSection
        <<< self.fromDateRow()
        <<< self.toDateRow()
        +++ self.chooseRidesSection
        self.addRidesRows()
    }
    
    private func addRidesRows() {
        if let lastForm = form.last {
            for (index, ride) in self.viewModel.rides.enumerated() {
                lastForm <<< ListCheckRow() {
                    $0.ride = ride.0
                    $0.value = ride.1
                    $0.disabled = Condition(booleanLiteral: self.viewModel.viewEditionDisabled.value)
                    }.onChange { row in
                        self.viewModel.updateSelection(at: index, value: row.value!)
                }
            }
        }
    }
    
    func updateRidesList() {
        self.viewModel.updateRidesList()
        self.chooseRidesSection.removeAll()
        self.addRidesRows()
    }
    
    func fromDateRow() -> DateRow {
        return DateRow(){
            $0.title = "From Date".localized()
            $0.value = self.viewModel.beginDate
            $0.disabled = Condition(booleanLiteral: self.viewModel.viewEditionDisabled.value)
            }.onChange({ [weak self] row in
                if let date = row.value,
                    let strongSelf = self {
                        strongSelf.viewModel.beginDate = date
                        strongSelf.updateRidesList()
                }
            })
    }
    
    func toDateRow() -> DateRow {
        return DateRow(){
            $0.title = "To Date".localized()
            $0.value = self.viewModel.endDate
            $0.disabled = Condition(booleanLiteral: self.viewModel.viewEditionDisabled.value)
            }.onChange({ [weak self] row in
                if let date = row.value,
                    let strongSelf = self {
                    strongSelf.viewModel.endDate = date
                    strongSelf.updateRidesList()
                }
            })
    }
}
