//
//  ReportViewController.swift
//  PDF
//
//  Created by Marcin Obolewicz on 12.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import UIKit

class ReportsViewController: UITableViewController {
    var viewModel: ReportsViewModelProtocol = ReportsViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Reports".localized()
        self.tableView.backgroundColor = UIColor.groupTableViewBackground
        self.tableView.tableFooterView = UIView()
        self.createReportButton()
        self.bindData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.viewModel.updateRidesList()
    }
    
    func bindData() {
        self.viewModel.reports.signal.observeValues { (value: [Report]?) in
            self.tableView.reloadData()
        }
    }
    
    func createReportButton() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "New".localized(), style: .plain, target: self, action: #selector(showReportView))
    }
    
    func showReportView() {
        self.viewModel.selectedReport = nil
        self.performSegue(withIdentifier: "reportView", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "reportView" {
            if let selectedReport = self.viewModel.selectedReport {
                if let viewController = segue.destination as? ReportDetailsViewController {
                    viewController.viewModel = ReportDetailsViewModel(report: selectedReport)
                }
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "REPORTS".localized()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.reports.value.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "reportCell", for: indexPath) as? ReportTableViewCell {
            cell.update(report: self.viewModel.reports.value[indexPath.row])
            return cell
        }
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.viewModel.deleteReport(at: indexPath.row)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewModel.selectedReport = self.viewModel.reports.value[indexPath.row]
        self.performSegue(withIdentifier: "reportView", sender: self)
    }
}
