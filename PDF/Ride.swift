//
//  Ride.swift
//  PDF
//
//  Created by Marcin Obolewicz on 11.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import RealmSwift
import CoreLocation

public class Ride : Object {
    public dynamic var rideId = 0
    public dynamic var beginDate = Date()
    public dynamic var endDate = Date()
    public dynamic var startAddress = ""
    public dynamic var endAddress = ""
    public dynamic var duration = 1
    public dynamic var distance = 0.0
    public dynamic var driverId: Int = 0
    public dynamic var vehicleId: Int = 0
    public dynamic var kilometerCostRate: Double = 0.0
    public dynamic var rideRouteDescription: String = ""
    public dynamic var tripPurpose: String = ""
    public dynamic var beginCarMileage: Double = 0.0
    public dynamic var endCarMilage: Double = 0.0
    
    public dynamic var avgVelocity: Double = 0.0
    public dynamic var minVelocity: Double = 0.0
    public dynamic var maxVelocity: Double = 0.0
    
    public var locations = List<Location>()
    
    override public class func primaryKey() -> String? {
        return "rideId"
    }
    
    convenience required public init(beginDate: Date, driverId: Int, vehicleId: Int){
        self.init()
        self.rideId = self.incrementID()
        self.beginDate = beginDate
        self.driverId = driverId
        self.vehicleId = vehicleId
    }
    
    convenience required public init(
        rideId: Int,
        driverId: Int,
        beginDate: Date,
        endDate: Date,
        duration: Int,
        vehicleId: Int,
        distance: Double){
        self.init()
        self.rideId = self.incrementID()
        self.driverId = driverId
        self.rideId = rideId
        self.distance = Double(distance)
        self.beginDate = beginDate
        self.endDate = endDate
        self.duration = duration
        self.vehicleId = vehicleId
        self.distance = Double(distance)
    }
    
    public func incrementID() -> Int {
        return self.incrementID(idProperty: "rideId", class: Ride.self)
    }
    
    public func calculateStatistics() {
        let minMaxVelocity = self.minMaxVelocity()
        let avarageVelocityMps = self.distance / Double(self.duration)
        self.avgVelocity = avarageVelocityMps*MpsToKmph/1000
        self.minVelocity = minMaxVelocity.0/1000
        self.maxVelocity = minMaxVelocity.1/1000
        
    }
    
    public func minMaxVelocity() -> (Double, Double) {
        var maxVelocity = 0.0
        var minVelocity = Double(Int.max)
        guard var previousLocation = self.locations.first else { return (0,0)}
        for location in locations {
            if location.timeStamp == previousLocation.timeStamp {
                continue
            }
            let v = previousLocation.velocityKmph(location: location)
            if v > maxVelocity {
                maxVelocity = v
            }
            if v < minVelocity {
                minVelocity = v
            }
            previousLocation = location
        }
        return (minVelocity, maxVelocity)
    }
}
