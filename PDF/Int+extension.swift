//
//  Int+extension.swift
//  PDF
//
//  Created by Marcin Obolewicz on 11.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import Foundation

extension Int{
    public func durationString() -> String{
        return String(format: "%02ld:%02ld:%02ld", self/(60*60), (self/60)%60, self%60)
    }
    
    public func durationMinutesString() -> String{
        return String(format: "%ld:%02ld:00", self/60, self%60)
    }
    
    public func percent(ofValue: Int) -> Int{
        if ofValue == 0{
            return 0
        }
        return Int(round(Double(self)/Double(ofValue)*100.0 ))
    }
    
    public func percent(ofValue: Double) -> Int{
        if ofValue == 0{
            return 0
        }
        return Int(round(Double(self)/ofValue*100.0 ))
    }
    
    public func multiplyByPercentage(percentageValue: Int) -> Int {
        return Int(round(Double(self * percentageValue)/100.0))
    }
    
}
