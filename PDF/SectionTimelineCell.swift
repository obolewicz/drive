//
//  SectionTimelineCell.swift
//  PDF
//
//  Created by Marcin Obolewicz on 16.02.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import UIKit

class SectionTimelineCell: UITableViewCell {
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var carImage: UIImageView!
    var date = Date()
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupCell(date: self.date)
    }
    
    func setupCell(date: Date) {
        self.date = date
        self.dayLabel.text = DateFormatterHelper.sharedInstance.reportFormatDate(date: date)
        let dayColor = UIColor.colorForWeekday(date: date)
        self.contentView.backgroundColor = dayColor.withAlphaComponent(0.4)
        self.carImage.layer.backgroundColor = dayColor.cgColor
    }
}
