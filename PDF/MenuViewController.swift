//
//  MenuViewController.swift
//  PDF
//
//  Created by Marcin Obolewicz on 21.02.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import Foundation
import SideMenuController

class MenuViewController: SideMenuController {
    

    required init?(coder aDecoder: NSCoder) {
        SideMenuController.preferences.drawing.sidePanelPosition = .overCenterPanelLeft
        SideMenuController.preferences.drawing.centerPanelShadow = true
        SideMenuController.preferences.drawing.menuButtonImage = UIImage(named: "menu")
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        performSegue(withIdentifier: "ride", sender: nil)
        performSegue(withIdentifier: "sideMenu", sender: nil)
    }
    
    override var prefersStatusBarHidden: Bool {
        get {
            return true
        }
    }
}
