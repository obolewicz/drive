//
//  ReportDetailsViewModelProtocol
//  PDF
//
//  Created by Marcin Obolewicz on 17.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import Foundation
import RealmSwift
import ReactiveCocoa
import ReactiveSwift

protocol ReportDetailsViewModelProtocol {
    var beginDate: Date { get set }
    var endDate: Date { get set }
    var filename: String { get set }
    var rides: [(Ride, Bool)] { get set }
    var viewEditionDisabled: MutableProperty<Bool> { get set }
    func createReport()
    var report: Report? { get set }
    func updateSelection(at index: Int, value: Bool)
    func updateRidesList()
    func createPDF()
}

class ReportDetailsViewModel: ReportDetailsViewModelProtocol {
    var viewEditionDisabled: MutableProperty<Bool>
    var beginDate: Date
    var endDate: Date
    var filename: String = "fileName"
    var rides: [(Ride, Bool)] = [(Ride, Bool)] ()
    
    var report: Report?
    
    var pdfCreator: PDFCreator?
    
    init(date: Date) {
        self.viewEditionDisabled = MutableProperty(false)
        self.beginDate = date.monthBeginDate()
        self.endDate = date.monthEndDate()
        self.updateRidesList()
    }
    
    init() {
        self.viewEditionDisabled = MutableProperty(false)
        self.beginDate = Date().monthBeginDate()
        self.endDate = Date().monthEndDate()
        self.updateRidesList()
    }
    
    init(report: Report) {
        self.report = report
        self.viewEditionDisabled = MutableProperty(true)
        self.beginDate = report.reportBeginDate
        self.endDate = report.reportEndDate
        self.updateReportsRidesList(report: report)
    }
    
    private func updateReportsRidesList(report: Report) {
        self.rides = Array(report.rides).map{($0, true)}
    }
    
    public func updateRidesList() {
        self.rides = self.getRides(driverId: DriverDataManager.shared.currentDriver.driverId).map{($0, true)}
    }
    
    private func getRides(driverId: Int) -> [Ride] {
        let realm = try! Realm()
        let dateFilter = NSPredicate(format:"driverId == %d && beginDate > %@ && endDate <= %@", driverId, self.beginDate as NSDate, self.endDate as NSDate)
        return realm.objects(Ride.self)
            .filter(dateFilter)
            .sorted(byProperty: "beginDate", ascending: true)
            .toArray(ofType: Ride.self)
    }
    
    func createReport() {
        let rides = self.rides
            .filter{$1 == true}
            .map({ (ride, selected) -> Ride in
            return ride
        })
        if let report = self.report {
            let realm = try! Realm()
            try! realm.write {
                realm.delete(report)
            }
        }
        
        let newReport = Report(companyId: DriverDataManager.shared.currentCompany.companyId, beginDate: self.beginDate, endDate: self.endDate, rides: rides)
        
        let realm = try! Realm()
        try! realm.write {
            DriverDataManager.shared.currentCompany.reports.append(newReport)
        }
        self.report = newReport
    }
    
    public func updateSelection(at index: Int, value: Bool) {
        self.rides[index].1 = value
    }
    
    public func createPDF() {
        if let report = report {
            self.pdfCreator = PDFCreator(htmlCreator: HtmlReportCreator(report: report, driver: self.reportDriver(), vehicle: self.reportVehicle(), company: self.reportCompany(), kilometerPrice: self.currentKilometerPrice()))
            self.pdfCreator!.createPDF(fileName: self.filename)
        }
    }
    
    private func reportVehicle() -> Vehicle {
        let realm = try! Realm()
        if let report = report {
            if let lastRide = report.rides.last {
                let idFilter = NSPredicate(format:"vehicleId == %d", lastRide.vehicleId)
                if let vehicle = realm.objects(Vehicle.self)
                    .filter(idFilter)
                    .first {
                    return vehicle
                }
            }
        }
        return Vehicle(autoincrementId: true)
    }
    
    private func reportDriver() -> Driver {
        return DriverDataManager.shared.currentDriver
    }
    
    private func reportCompany() -> Company {
        return DriverDataManager.shared.currentCompany
    }
    
    private func currentKilometerPrice() -> Double {
        return KilometerCostManager.currentKilometerCost()
    }
}
