//
//  HtmlReportCreator.swift
//  PDF
//
//  Created by Marcin Obolewicz on 03.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import Foundation

//Report tags
let DATE_BEGIN = "{DATE_BEGIN}"
let MILAGE_BEGIN = "{MILAGE_BEGIN}"
let DATE_END = "{DATE_END}"
let MILAGE_END = "{MILAGE_END}"
let REGISTRATION_NUMBER = "{REGISTRATION_NUMBER}"
let COMPANY_ADDRESS = "{COMPANY_ADDRESS}"
let ENGINE_DISPLACEMENT = "{ENGINE_DISPLACEMENT}"
let SUM_KM = "{SUM_KM}"
let PRICE = "{PRICE}"
let SUM_PRICE = "{SUM_PRICE}"

let RIDE_ROWS = "{RIDE_ROWS}"
// Single row tags
let ROW_NUMBER = "{NR}"
let RIDE_DATE = "{RIDE_DATE}"
let BEGIN_MILAGE = "{BEGIN_MILAGE}"
let END_MILATE = "{END_MILAGE}"
let DESCRIPTIOIN = "{DESCRIPTION}"
let PURPOSE = "{PURPOSE}"
let DISTANCE = "{DISTANCE}"
let VALUE = "{VALUE}"
let DRIVER = "{DRIVER_NAME}"

class HtmlReportCreator: NSObject, HtmlReportCreatorProtocol {
    let report: Report
    let path = Bundle.main.path(forResource: "report", ofType: "html")
    let singleRouteHTMLTemplatePath = Bundle.main.path(forResource: "report_row", ofType: "html")
    var kilometerPrice = 0.85
    let driver: Driver
    let vehicle: Vehicle
    let company: Company
    
    init(report: Report, driver: Driver, vehicle: Vehicle, company: Company, kilometerPrice: Double) {
        self.driver = driver
        self.report = report
    print(self.report.rides.count)
        self.vehicle = vehicle
        self.company = company
        self.kilometerPrice = kilometerPrice
        super.init()
    }
    
    func createHtmlReport() -> String {
        let report = self.renderReport()
        return report
    }
    
    
    func renderReport() -> String {
        guard let path = self.path else { return ""}
        do {
            var HTMLContent = try String(contentsOfFile: path)
            self.replaceTagsWithValues(HTMLContent: &HTMLContent)
            
            var allItems = ""
            for (index, ride) in report.rides.enumerated() {

                allItems += self.replaceRowTagsWithValues(rowIndex: index, ride: ride)
            }
            HTMLContent = HTMLContent.replacingOccurrences(of: RIDE_ROWS, with: allItems)
            return HTMLContent
        }
        catch {
            print("Unable to open and use HTML template files.")
        }
        return ""
    }
    
    private func replaceTagsWithValues(HTMLContent: inout String) {
        HTMLContent = HTMLContent.replacingOccurrences(of: DATE_BEGIN, with: DateFormatterHelper.sharedInstance.reportFormatDate(date:self.report.reportBeginDate))
        HTMLContent = HTMLContent.replacingOccurrences(of: MILAGE_BEGIN, with: self.mileageFirstBegin())
        HTMLContent = HTMLContent.replacingOccurrences(of: DATE_END, with: DateFormatterHelper.sharedInstance.reportFormatDate(date:self.report.reportEndDate))
        HTMLContent = HTMLContent.replacingOccurrences(of: MILAGE_END, with: self.mileageLastEnd())
        HTMLContent = HTMLContent.replacingOccurrences(of: REGISTRATION_NUMBER, with: self.vehicle.registrationNumber)
        HTMLContent = HTMLContent.replacingOccurrences(of: ENGINE_DISPLACEMENT, with: String(self.vehicle.engineDisplacement))
        HTMLContent = HTMLContent.replacingOccurrences(of: COMPANY_ADDRESS, with: self.company.fullAddress())
        HTMLContent = HTMLContent.replacingOccurrences(of: SUM_KM, with: self.report.calculateSumDistance().distanceFormatted())
        HTMLContent = HTMLContent.replacingOccurrences(of: PRICE, with: self.kilometerPrice.priceFormatted())
        HTMLContent = HTMLContent.replacingOccurrences(of: SUM_PRICE, with: self.report.calculateSumPrice(kilometerPrice: self.kilometerPrice).priceFormatted())
    }
    
    private func replaceRowTagsWithValues(rowIndex: Int, ride: Ride) -> String {
        do {
            guard let singleRouteHTMLTemplatePath = self.singleRouteHTMLTemplatePath else { return ""}
            var itemHTMLContent = try String(contentsOfFile: singleRouteHTMLTemplatePath)
            
            itemHTMLContent = itemHTMLContent.replacingOccurrences(of: ROW_NUMBER, with: "\(rowIndex+1)")
            itemHTMLContent = itemHTMLContent.replacingOccurrences(of: RIDE_DATE, with: DateFormatterHelper.sharedInstance.reportFormatDate(date: ride.beginDate))
            itemHTMLContent = itemHTMLContent.replacingOccurrences(of: BEGIN_MILAGE, with: ride.beginCarMileage.distanceFormatted())
            itemHTMLContent = itemHTMLContent.replacingOccurrences(of: END_MILATE, with: ride.endCarMilage.distanceFormatted())
            itemHTMLContent = itemHTMLContent.replacingOccurrences(of: PRICE, with: kilometerPrice.priceFormatted())
            itemHTMLContent = itemHTMLContent.replacingOccurrences(of: DESCRIPTIOIN, with: ride.rideRouteDescription)
            itemHTMLContent = itemHTMLContent.replacingOccurrences(of: PURPOSE, with: ride.tripPurpose)
            itemHTMLContent = itemHTMLContent.replacingOccurrences(of: DISTANCE, with: ride.distance.distanceFormatted())
            itemHTMLContent = itemHTMLContent.replacingOccurrences(of: VALUE, with: (kilometerPrice*ride.distance/1000.0).priceFormatted())
            itemHTMLContent = itemHTMLContent.replacingOccurrences(of: DRIVER, with: self.driver.fullName())
            return itemHTMLContent
        }
        catch {
            print("Unable to open and use HTML row template files.")
        }
        return ""
    }
    
    private func mileageFirstBegin() -> String {
        if let firstRide = self.report.rides.first {
            return firstRide.beginCarMileage.distanceFormatted()
        }
        return 0.0.distanceFormatted()
    }
    
    private func mileageLastEnd() -> String {
        if let lastRide = self.report.rides.last {
            return lastRide.endCarMilage.distanceFormatted()
        }
        return 0.0.distanceFormatted()
    }
}
