//
//  UIImage+extension.swift
//  PDF
//
//  Created by Marcin Obolewicz on 11.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import UIKit

extension UIImage{
    public func saveUserImage(name: String) -> Bool{
        let imageData = NSData(data:UIImagePNGRepresentation(self)!)
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let docs: NSString = paths[0] as NSString
        let fullPath = docs.appendingPathComponent(name)
        return imageData.write(toFile: fullPath, atomically: true)
    }
    
    public class func readUserImage(name: String) -> UIImage?{
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let docs: NSString = paths[0] as NSString
        let fullPath = docs.appendingPathComponent(name)
        let url = NSURL(fileURLWithPath: fullPath)
        if let imageData = NSData(contentsOf: url as URL){
            if let image = UIImage(data: imageData as Data){
                return image
            }
        }
        
        return nil
    }
    
    public func imageResize(sizeChange:CGSize)-> UIImage {
        let hasAlpha = true
        let scale: CGFloat = 0.0
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        self.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
}

