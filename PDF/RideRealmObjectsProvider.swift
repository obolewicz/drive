//
//  RideRealmObjectsProvider.swift
//  PDF
//
//  Created by Marcin Obolewicz on 11.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import Foundation
import RealmSwift

public protocol HistoryObjectProtocol {
    var itemId: Int { get }
    var title: String { get }
    var details: String { get }
    var date: Date { get }
}

extension Ride: HistoryObjectProtocol {
    public var itemId: Int {
        get {
            return self.rideId
        }
    }
    public var title: String {
        get {
            return DateFormatterHelper.sharedInstance.timeStringFromDate(date: self.beginDate)
        }
    }
    public var details: String {
        get {
            return self.duration.durationString()
        }
    }
    public var date: Date {
        get {
            return self.beginDate
        }
    }
}

public protocol RealmObjectsProviderProtocol {
    func objectsInMonth(_ date: Date) -> [HistoryObjectProtocol]
    func objectsInDay(_ date: Date) -> [HistoryObjectProtocol]
    func objectsCountInDay(_ date: Date) -> Int
}

public class RideRealmObjectsProvider: RealmObjectsProviderProtocol {
    public init(){}
    
    public func objectsInMonth(_ date: Date) -> [HistoryObjectProtocol] {
        return self.getRidesInMonthWithDate(date: date, driverId: DriverDataManager.shared.currentDriver.driverId)
    }
    
    public func objectsInDay(_ date: Date) -> [HistoryObjectProtocol] {
        return self.getRidesInDayWithDate(date: date, driverId: DriverDataManager.shared.currentDriver.driverId)
    }
    
    public func objectsCountInDay(_ date: Date) -> Int {
        return self.getRidesInDayWithDate(date: date, driverId: DriverDataManager.shared.currentDriver.driverId).count
    }
    
    public func getRidesInMonthWithDate(date: Date, driverId: Int) -> Array<Ride> {
        let realm = try! Realm()
        let dateFilter = NSPredicate(format:"driverId == %d && beginDate > %@ && endDate <= %@", driverId, date.monthBeginDate() as NSDate, date.monthEndDate() as NSDate)
        return realm.objects(Ride.self)
            .filter(dateFilter)
            .sorted(byProperty: "beginDate", ascending: true)
            .toArray(ofType: Ride.self)
    }
    
    public func getRidesInDayWithDate(date: Date, driverId: Int) -> Array<Ride> {
        let realm = try! Realm()
        let dateFilter = NSPredicate(format:"driverId == %d && beginDate > %@ && endDate <= %@", driverId, date.dayBeginDate() as NSDate, date.dayEndDate() as NSDate)
        return realm.objects(Ride.self)
            .filter(dateFilter)
            .sorted(byProperty: "beginDate", ascending: true)
            .toArray(ofType: Ride.self)
    }
}
