//
//  CreateRideViewController.swift
//  PDF
//
//  Created by Marcin Obolewicz on 11.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import UIKit
import RealmSwift
import MapKit
import ReactiveCocoa
import ReactiveSwift

class CreateRideViewController: UIViewController {
    let viewModel: RideViewModelProtocol = RideViewModel()
    
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!

    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var pauseButton: UIButton!
    @IBOutlet weak var resumeButton: UIButton!
    @IBOutlet weak var endButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.map.delegate = self
        self.title = "Create Ride".localized()
        self.bindButtons()
        self.setupObservers()
        self.setupMap()

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let stateValue = self.viewModel.rideState.value {
            if let rideState = RideState(rawValue: stateValue) {
            self.updateButtons(state: rideState)
            }
        }
        if let duration:Double = self.viewModel.durationValue.value {
            self.updateDurationLabel(duration: duration)
        }
        if let distance:Double = self.viewModel.distanceValue.value {
            self.updateDistanceLabel(distance: distance)
        }
    }
    
    private func bindButtons() {
        self.startButton.reactive.pressed = CocoaAction(self.viewModel.startAction)
        self.resumeButton.reactive.pressed = CocoaAction(self.viewModel.resumeAction)
        self.pauseButton.reactive.pressed = CocoaAction(self.viewModel.pauseAction)
        self.endButton.reactive.pressed = CocoaAction(self.viewModel.stopAction)
    }
    
    private func setupObservers() {
        self.viewModel.polyline.signal.observeValues { (value: MKPolyline?) in
            if let polyline = value {
                self.map.add(polyline)
            }
        }
        self.viewModel.rideState.signal.observeValues { (value: Int?) in
            if value != nil {
                if let state: RideState = RideState(rawValue: value!) {
                    self.updateButtons(state: state)
                }
            }
        }
        
        self.viewModel.distanceValue.signal.observeValues { (value: Double?) in
            if let distance = value {
                self.updateDistanceLabel(distance: distance)
            }
        }
        
        self.viewModel.durationValue.signal.observeValues { (value: Double?) in
            if let duration = value {
                self.updateDurationLabel(duration: duration)
            }
        }
        
        self.viewModel.locationSignal.observeValues { (result: (current: Location, before: Location?)) in
            if let locationBefore: Location = result.before {
                let polyline = MKPolyline(coordinates: [locationBefore.coordinate(), result.current.coordinate()], count: 2)
                self.map.add(polyline)
            }
        }
    }
    
    private func setupMap(){
        self.map.delegate = self
        self.map.showsUserLocation = true
        self.map.userTrackingMode = MKUserTrackingMode.follow
        self.map.mapType = MKMapType.standard
    }
    
    private func updateDurationLabel(duration: Double) -> Void {
        self.durationLabel.text = duration.durationString()
    }
    
    private func updateDistanceLabel(distance: Double) -> Void {
        self.distanceLabel.text = String(format: "%1.2f", (distance / 1000.0))
    }
    
    private func updateButtons(state: RideState) -> Void {
        switch state {
        case .paused:
            self.pauseButton.isHidden = true
            self.startButton.isHidden = true
            self.resumeButton.isHidden = false
            self.endButton.isHidden = false
        case .running:
            self.startButton.isHidden = true
            self.resumeButton.isHidden = true
            self.endButton.isHidden = true
            self.pauseButton.isHidden = false
        case .stopped:
            self.pauseButton.isHidden = true
            self.resumeButton.isHidden = true
            self.endButton.isHidden = true
            self.startButton.isHidden = false
        }
    }
}

extension CreateRideViewController: MKMapViewDelegate{
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        return MKPolylineRenderer.customOverlayRenderer(overlay)
    }
}
