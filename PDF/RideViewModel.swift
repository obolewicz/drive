//
//  RideViewModel.swift
//  PDF
//
//  Created by Marcin Obolewicz on 12.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import UIKit
import ReactiveCocoa
import ReactiveSwift
import Result
import MapKit
import RealmSwift

internal protocol RideViewModelProtocol {
    var locationSignal: Signal<(current: Location, previous: Location?), NoError> { get }
    var rideState: DynamicProperty<Int> { get }
    var durationValue: DynamicProperty<Double> { get }
    var distanceValue: DynamicProperty<Double> { get }
    var startAction: Action<Void, Void, NoError> { get }
    var pauseAction: Action<Void, Void, NoError> { get }
    var resumeAction: Action<Void, Void, NoError> { get }
    var stopAction: Action<Void, Void, NoError>! { get }
    var polyline: DynamicProperty<MKPolyline> { get }
}

internal class RideViewModel: NSObject, RideViewModelProtocol {
    
    internal var locationSignal: Signal<(current: Location, previous: Location?), NoError> {
        return RideManager.shared.locationSignal
    }
    
    internal var durationValue: DynamicProperty<Double>
    internal var distanceValue: DynamicProperty<Double>
    internal var rideState: DynamicProperty<Int>
    
    internal var startAction: Action<Void, Void, NoError>
    internal var pauseAction: Action<Void, Void, NoError>
    internal var resumeAction: Action<Void, Void, NoError>
    internal var stopAction: Action<Void, Void, NoError>!
    internal var addressUpdateAction: Action<String, Void, NoError>
    internal var polyline: DynamicProperty<MKPolyline>
    
    internal var beginAddressAction: Action<String, Void, NoError>!
    internal var endAddressAction: Action<String, Void, NoError>!
    
    override init() {
        self.polyline = DynamicProperty<MKPolyline>(object: RideManager.shared.locationManager, keyPath: "polyline")
        self.rideState = DynamicProperty<Int>(object: RideManager.shared, keyPath: "state")
        self.distanceValue = DynamicProperty<Double>(object: RideManager.shared, keyPath: "distance")
        self.durationValue = DynamicProperty<Double>(object: RideManager.shared, keyPath: "duration")
        
        self.startAction = Action<Void, Void, NoError> { _ in
            return SignalProducer<Void, NoError> { observer, _ in
                RideManager.shared.start()
                observer.sendCompleted()
            }
        }
        
        self.pauseAction = Action<Void, Void, NoError> { _ in
            return SignalProducer<Void, NoError> { observer, _ in
                RideManager.shared.pause()
                observer.sendCompleted()
            }
        }
        
        self.resumeAction = Action<Void, Void, NoError> { _ in
            return SignalProducer<Void, NoError> { observer, _ in
                RideManager.shared.resume()
                observer.sendCompleted()
            }
        }
        
        
        
        self.addressUpdateAction = Action<String, Void, NoError> {
            (location: String) in
            return SignalProducer<Void, NoError> { observer, _ in
                RideManager.shared.rideRouteDescriptionUpdate(location: location)
                observer.sendCompleted()
            }
        }
        
        super.init()
        self.bindAddressAction()
        self.stopAction = Action<Void, Void, NoError> { _ in
            return SignalProducer<Void, NoError> { observer, _ in
                self.rideStatisticsCalculate()
                RideManager.shared.stop()
                self.getStartAddressDetails()
                self.getStopAddressDetails()
                observer.sendCompleted()
            }
        }
    }
    
    func rideStatisticsCalculate() {
        Realm.update(completion: {
            RideManager.shared.ride?.calculateStatistics()
        })
    }
    
    func bindAddressAction() {
        self.beginAddressAction = Action<String, Void, NoError> {
            (addressDescription: String) in
            return SignalProducer<Void, NoError> { observer, _ in
                if let ride = RideManager.shared.ride {
                    Realm.update(completion: {
                        ride.startAddress = addressDescription
                    })
                }
            }
        }
        
        self.endAddressAction = Action<String, Void, NoError> {
            (addressDescription: String) in
            return SignalProducer<Void, NoError> { observer, _ in
                if let ride = RideManager.shared.ride {
                    Realm.update(completion: {
                        ride.endAddress = addressDescription
                    })
                }
            }
        }
    }
    
    func getStartAddressDetails() {
        if let ride = RideManager.shared.ride {
            if ride.locations.count > 2 {
                self.geocodeLocation(location: ride.locations.first!, action: self.beginAddressAction)
            }
        }
    }
    
    func getStopAddressDetails() {
        if let ride = RideManager.shared.ride {
            if ride.locations.count > 2 {
                self.geocodeLocation(location: ride.locations.last!, action: self.endAddressAction)
            }
        }
    }
    
    public func geocodeLocation(location: Location, action: Action<String, Void, NoError>) {
        let location = CLLocation(latitude: location.latitude, longitude: location.longitude)
        
        CLGeocoder().reverseGeocodeLocation(location) { (placemarks, error) in
            if error == nil {
                if let placemarks = placemarks, let placemark = placemarks.first {
                    if let street = placemark.thoroughfare,
                        let city = placemark.locality {
                        action.consume(street + ", " + city)
                    }
                }
            }
        }
    }

}

