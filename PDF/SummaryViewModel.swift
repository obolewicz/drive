//
//  SummaryViewModel.swift
//  PDF
//
//  Created by Marcin Obolewicz on 12.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import UIKit
import RealmSwift

protocol RideSummaryProtocol {
    var durationDisplay: String { get }
    var distanceDisplay: String { get }
    var routeDescrioptionDispaly: String { get }
    var locationsDisplay: List<Location> { get }
}

extension Ride: RideSummaryProtocol {
    public var durationDisplay: String {
        get {
            return self.duration.durationString()
        }
    }
    public var distanceDisplay: String {
        get {
            return self.distance.distanceFormatted() 
        }
    }
    public var routeDescrioptionDispaly: String {
        get {
            return self.rideRouteDescription
        }
    }
    public var locationsDisplay: List<Location> {
        get {
            return self.locations
        }
    }
}

protocol SummaryViewModelProtocol {
    var selectedItem: RideSummaryProtocol? { get set }
}

class SummaryViewModel: NSObject, SummaryViewModelProtocol {
    internal var selectedItem: RideSummaryProtocol?
}
