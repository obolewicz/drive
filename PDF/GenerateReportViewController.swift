//
//  ViewController.swift
//  PDF
//
//  Created by Marcin Obolewicz on 03.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import UIKit
import MessageUI
import RealmSwift

class GenerateReportViewController: UIViewController {

    var viewModel: ReportDetailsViewModelProtocol!
    
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "PDF Report".localized()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.viewModel.createPDF()
        self.loadPDF(filename: self.viewModel.filename)
        self.setupRightButtonShare()
    }
    
    func setupRightButtonShare() {
        let sharePDFButton = UIBarButtonItem(title: "Share".localized(), style: .plain, target: self, action: #selector(sharePDFAction))
        self.navigationItem.rightBarButtonItem = sharePDFButton
    }
    
    func sharePDFAction() {
        self.share(fileName: self.viewModel.filename)
    }
    
    func loadPDF(filename: String) {
        let filePath = filename.docunmetPath()
        let url = NSURL(fileURLWithPath: filePath)
        let urlRequest = NSURLRequest(url: url as URL)
        self.webView.loadRequest(urlRequest as URLRequest)
    }
    
    // MARK: Sharesheet
    func share(fileName: String) {
        if let data = NSData(contentsOfFile: fileName.docunmetPath()) {
            let objectsToShare = [data]
            let activityViewController = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            self.present(activityViewController, animated: true, completion: nil)
        }
    }
}

