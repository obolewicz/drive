//
//  HistoryViewModel.swift
//  DriveMate
//
//  Created by Marcin Obolewicz on 10.01.2017.
//  Copyright © 2017 Luxmed. All rights reserved.
//

import UIKit
import ReactiveCocoa
import ReactiveSwift
import Result

internal protocol HistoryViewModelProtocol {
    var items: [HistoryObjectProtocol] { get set }
    var selectedDayItems: [HistoryObjectProtocol] { get set }
    var summaryAction: Action<HistoryObjectProtocol, Void, NoError>! { get set }
    var selectedMonth: Date { get set }
    var selectedMonthString: MutableProperty<String> { get }
    func setNextMonth()
    func setPrevMonth()
    func itemsInMonth(_ date: Date) -> [HistoryObjectProtocol]
    func itemsInDay(_ date: Date) -> [HistoryObjectProtocol]
    func itemsCountInDay(_ date: Date) -> Int
    func daysWithRidesCount() -> Int
    func cellContent(index: Int) -> (Date?, HistoryObjectProtocol?)
}

class HistoryViewModel: NSObject, HistoryViewModelProtocol {
    internal var items: [HistoryObjectProtocol] = []
    internal var selectedDayItems: [HistoryObjectProtocol] = []
    internal var summaryAction: Action<HistoryObjectProtocol, Void, NoError>!
    internal var realmObjectsProvider: RealmObjectsProviderProtocol
    internal var selectedMonthString = MutableProperty("")
    internal var selectedMonth: Date {
        didSet{
            self.items = self.itemsInMonth(self.selectedMonth)
            self.selectedMonthString.value = DateFormatterHelper.sharedInstance.monthYearJournalStringFromDate(date: selectedMonth)
        }
    }
    
    override init() {
        self.realmObjectsProvider = RideRealmObjectsProvider()
        self.selectedMonth = Date()
        super.init()
        self.bindAction()
    }
    
    func bindAction() {
        self.summaryAction = Action<HistoryObjectProtocol, Void, NoError> {
            (input: HistoryObjectProtocol) in
            return SignalProducer<Void, NoError> { observer, _ in
                UINavigationController.pushSummaryView(input)
                observer.sendCompleted()
            }
        }
    }
    
    internal func setNextMonth() {
        if let next = self.selectedMonth.nextMonth() {
            self.selectedMonth = next
        }
    }
    
    internal func setPrevMonth() {
        if let prev = self.selectedMonth.previousMonth() {
            self.selectedMonth = prev
        }
    }
    
    internal func itemsInMonth(_ date: Date) -> [HistoryObjectProtocol] {
        return realmObjectsProvider.objectsInMonth(date)
    }
    
    internal func itemsInDay(_ date: Date) -> [HistoryObjectProtocol] {
        return self.items.filter{ $0.date.isSameDay(day: date) }
    }
    
    internal func itemsCountInDay(_ date: Date) -> Int {
        return self.itemsInDay(date).count
    }
    
    internal func daysWithRidesCount() -> Int {
        var result: Int = 1
        guard self.items.count != 0 else { return 0 }
        guard self.items.count != 1 else { return 1 }
        var previousRideDate = self.items.first!.date
        for ride in self.items {
            if ride.itemId == self.items.first!.itemId { continue }
            if !ride.date.isSameDay(day: previousRideDate) {
                result += 1
            }
            previousRideDate = ride.date
        }
        return result
    }
    
    func cellContent(index: Int) -> (Date?, HistoryObjectProtocol?) {
        if let first = self.items.first {
            if index == 0 {
                return (first.date, nil)
            }
            if index == 1 {
                return (nil,first)
            }
            var sectionsCount = 1
            var sectionDate = first.date
            for (rideIndex, ride) in self.items.enumerated() {
                if rideIndex == 0 { continue }
                if !ride.date.isSameDay(day: sectionDate) {
                    if sectionsCount + rideIndex == index {
                        return (ride.date, nil)
                    }
                    sectionsCount += 1
                    sectionDate = ride.date
                }
                if sectionsCount + rideIndex == index {
                    return (nil, ride)
                }
            }
        }
        return (nil, nil)
    }
}
