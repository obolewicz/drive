//
//  DateFormatterHelper.swift
//  Pods
//
//  Created by Marcin on 19.07.2016.
//
//

import UIKit
import Localize_Swift

public enum UnitSystem : String {
    case Metric = "Metric", Imperial = "Imperial"
    public static let allValues = [Metric.rawValue, Imperial.rawValue]
    public static let allValuesLocalized = [Metric.rawValue.localized(), Imperial.rawValue.localized()]
}

public enum FirstDayOfWeek : String {
    case Monday = "Monday", Saturday = "Saturday", Sunday = "Sunday"
    public static let allValues = [Monday.rawValue, Saturday.rawValue, Sunday.rawValue]
    public static let allValuesLocalized = [Monday.rawValue.localized(), Saturday.rawValue.localized(), Sunday.rawValue.localized()]
}

public enum DateFormat : String {
    case ddmmyyyy = "ddmmyyyy", mmddyyyy = "mmddyyyy", yyyymmdd = "yyyymmdd"
    public static let allValues = [ddmmyyyy.rawValue, mmddyyyy.rawValue, yyyymmdd.rawValue]
    public static let allValuesLocalized = [ddmmyyyy.rawValue.localized(), mmddyyyy.rawValue.localized(), yyyymmdd.rawValue.localized()]
}

public enum TimeFormat : String {
    case H24 = "h24", H12 = "h12"
    public static let allValues = [H24.rawValue, H12.rawValue]
    public static let allValuesLocalized = [H24.rawValue.localized(), H12.rawValue.localized()]
}

public let DateDayZero = "2015-12-30T15:35:00Z"

private let sharedDateFormatterHelper = DateFormatterHelper()

public class DateFormatterHelper: NSObject {
    
    private let dateDisplayFormat = "dd MMMM yyyy"
    private let dateYear = "yyyy"
    
    private let dateIsoFormatNoMiliseconds = "yyyy-MM-dd'T'HH:mm:ssZ"
    private let dateIsoFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    private let dateIsoFilteringMonthFormat = "yyyy-MM"
    private let dateIsoFilteringDayFormat = "yyyy-MM-dd"
    
    public class var sharedInstance: DateFormatterHelper{
        get
        {
            return sharedDateFormatterHelper
        }
    }
    
    //    Singleton private initializer
    internal override init() {}
    
    public func isoDateStringCurrentZoneFromUTC(dateUTC: String) -> String {
        if let date = self.isoDateFromString(dateString: dateUTC) {
            let timeZone = TimeZone.ReferenceType.local
            formatter.timeZone = timeZone
            formatter.dateFormat = self.dateIsoFormat
            return formatter.string(from: date)
        }
        return ""
    }
    
    public func calculateAgeWithStringDate(birthDate: String) -> Int {
        if let date = DateFormatterHelper.sharedInstance.birthDateFromString(dateString: birthDate) {
            return calculateAgeWithDate(date: date)
        }
        return 0
    }
    
    public func calculateAgeWithDate(date: Date) -> Int {
        let dateComponents = Calendar.current.dateComponents([.year], from: date, to: Date())
        return dateComponents.year!
    }
    
    lazy var formatter: DateFormatter = {
        return DateFormatter()
    }()
    
    public func reportFormatDate(date: Date) -> String {
        formatter.dateFormat = self.dateDisplayFormat
        return formatter.string(from: date as Date)
    }

    
    private func numberToMonth(date: Date) -> String {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.month], from: date)
        let month = components.month
        let months = self.formatter.standaloneMonthSymbols
        return months![month!-1]
    }
    
    public func birthDateFromString(dateString: String) -> Date? {
        formatter.dateFormat = self.dateIsoFormat
        return formatter.date(from: dateString)
    }
    
    public func dateFromJSONString(dateString: String) -> Date? {
        formatter.dateFormat = self.dateIsoFormatNoMiliseconds
        return formatter.date(from: dateString)
    }
    
    public func dateJSONStringFromDate(date: Date) -> String {
        formatter.dateFormat = self.dateIsoFormatNoMiliseconds
        return formatter.string(from: date as Date)
    }
    
    public func timeStringFromStringTimeStamp(dateString: String) -> String? {
        formatter.dateFormat = self.dateIsoFormat
        if let date = formatter.date(from: dateString){
            formatter.dateFormat = self.dateDisplayFormat
            return formatter.string(from: date)
        }
        return ""
    }
    
    public func isoDateFromString(dateString: String) -> Date? {
        if let timeZone = TimeZone(identifier: "UTC") {
            return self.isoDateFromString(dateString: dateString, timeZone: timeZone)
        }
        return nil
    }
    
    private func isoDateLocalFromString(dateString: String) -> Date? {
        let timeZone = TimeZone.ReferenceType.local
        return self.isoDateFromString(dateString: dateString, timeZone: timeZone)
    }
    
    private func isoDateFromString(dateString: String, timeZone: TimeZone) -> Date? {
        formatter.timeZone = timeZone as TimeZone!
        formatter.dateFormat = self.dateIsoFormat
        if let date = formatter.date(from: dateString) {
            return date as Date?
        }
        else {
            formatter.dateFormat = self.dateIsoFormatNoMiliseconds
            return formatter.date(from: dateString)
        }
    }
    
    public func isoStringFromDate(date: Date) -> String {
        let timeZone = TimeZone(identifier: "UTC")
        formatter.timeZone = timeZone
        formatter.dateFormat = self.dateIsoFormat
        return formatter.string(from: date)
    }
    
    public func isoFilteringMonthStringFromDate(date: Date) -> String {
        let timeZone = TimeZone(identifier: "UTC")
        formatter.timeZone = timeZone
        formatter.dateFormat = self.dateIsoFilteringMonthFormat
        return formatter.string(from: date)
    }
    
    public func isoFilteringDayStringFromDate(date: Date) -> String {
        let timeZone = TimeZone(identifier: "UTC")
        formatter.timeZone = timeZone
        formatter.dateFormat = self.dateIsoFilteringDayFormat
        return formatter.string(from: date)
    }
    
    public func fromString(dateString:String) -> Date{
        self.formatter.dateFormat = dateIsoFormat
        return self.formatter.date(from: dateString)!
    }
    
    public func toString(date: Date) -> String{
        self.formatter.dateFormat = dateIsoFormat
        self.formatter.timeZone = TimeZone(identifier: "UTC")
        return self.formatter.string(from: date)
    }
    
    public func timeStringFromDate(date: Date) -> String {
        formatter.dateFormat = "HH:mm:ss"
        formatter.timeZone = NSTimeZone.local
        return formatter.string(from: date)
    }
    
    public func monthYearJournalStringFromDate(date: Date) -> String {
        formatter.dateFormat = "dd.MM.yyyy"
        formatter.timeZone = NSTimeZone.local
        return formatter.string(from: date as Date)
    }
    
    public func rideBeginString(date: Date) -> String {
        formatter.dateFormat = "dd.MM.yyyy - HH:mm"
        formatter.timeZone = NSTimeZone.local
        return formatter.string(from: date)
    }
}

extension Date {
    static func dayZero() -> Date {
        return DateFormatterHelper.sharedInstance.isoDateFromString(dateString: DateDayZero)!
    }
}
