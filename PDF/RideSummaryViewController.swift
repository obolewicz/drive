//
//  RideSummaryViewController.swift
//  PDF
//
//  Created by Marcin Obolewicz on 12.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import UIKit
import MapKit

class RideSummaryViewController: UIViewController {
    var viewModel: SummaryViewModelProtocol = SummaryViewModel()
    
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var distanceLabel:UILabel!
    @IBOutlet weak var routeDescriptionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Ride Summary".localized()
        self.mapView.delegate = self
        if let item = self.viewModel.selectedItem {
            self.updateDetails(item: item)
            self.updateMapView(ride: item)
        }
    }
    
    func updateDetails(item: RideSummaryProtocol) {
        self.durationLabel.text = item.durationDisplay
        self.distanceLabel.text = item.distanceDisplay
        self.routeDescriptionLabel.text = item.routeDescrioptionDispaly
    }
    
    private func updateMapView(ride: RideSummaryProtocol) {
        if ride.locationsDisplay.count > 0 {
            self.mapView.drawRoute(ride: ride)
        }
    }
    
    func hideContentController() {
        self.hideViewController()
    }
    
    func controllerIndex() -> Int {
        return 2
    }
}

extension RideSummaryViewController: MKMapViewDelegate{
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        return MKPolylineRenderer.customOverlayRenderer(overlay)
    }
}
