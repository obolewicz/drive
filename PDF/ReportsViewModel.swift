//
//  ReportsListViewModel.swift
//  PDF
//
//  Created by Marcin Obolewicz on 21.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import Foundation
import RealmSwift
import ReactiveSwift

protocol ReportsViewModelProtocol {
    var reports: MutableProperty<[Report]>{ get set }
    var selectedReport: Report? { get set }
    func deleteReport(at index: Int)
    func updateRidesList()
}

class ReportsViewModel: ReportsViewModelProtocol {
    var reports = MutableProperty([Report]())
    var selectedReport: Report?
    
    init() {
        self.updateRidesList()
    }
    
    func updateRidesList() {
        self.reports.value = self.getCompanyReports(companyId: DriverDataManager.shared.currentCompany.companyId)
    }
    
    private func getCompanyReports(companyId: Int) -> [Report] {
        return Array(DriverDataManager.shared.currentCompany.reports)
    }
    
    func deleteReport(at index: Int) {
        let deleteReport = self.reports.value[index]
        let realm = try! Realm()
        try! realm.write {
            realm.delete(deleteReport)
        }
        self.reports.value.remove(at: index)
    }
}
