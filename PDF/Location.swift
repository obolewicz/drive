//
//  Location.swift
//  PDF
//
//  Created by Marcin Obolewicz on 12.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import RealmSwift
import CoreLocation
import MapKit

public let MpsToKmph = 3.6

public class Location: Object {
    public dynamic var latitude = 0.0
    public dynamic var longitude = 0.0
    public dynamic var timeStamp = 0.0
    public dynamic var altitude = 0.0
    public dynamic var distance = 0.0
    public dynamic var hasNext = true
    
    convenience required public init(timestamp: Double, distance: Double, latitude: Double, longitude: Double, altitude: Double) {
        self.init()
        self.timeStamp = timestamp
        self.longitude = longitude
        self.latitude = latitude
        self.distance = distance
        self.altitude = altitude
    }
    
    convenience required public init(location: CLLocation, previousLocation: CLLocation? = nil){
        self.init()
        self.timeStamp = NSDate().timeIntervalSince1970
        self.longitude = location.coordinate.longitude
        self.latitude = location.coordinate.latitude
        self.altitude = location.altitude
        if let pl = previousLocation {
            self.distance = location.distance(from: pl)
        }
    }
    
    private func location() -> CLLocation{
        return CLLocation(latitude: self.latitude, longitude:self.longitude)
    }
    
    public func coordinate() -> CLLocationCoordinate2D{
        return CLLocationCoordinate2D(latitude: self.latitude, longitude: self.longitude)
    }
    
    public func distanceMeters(location: Location) -> CLLocationDistance {
        let point1 = MKMapPointForCoordinate(self.coordinate())
        let point2 = MKMapPointForCoordinate(location.coordinate())
        return MKMetersBetweenMapPoints(point1, point2);
    }
    
    private func velocityMetersPerSecond(location: Location) -> Double {
        let distance = self.distance
        let time = self.durationSeconds(location: location)
        if time == 0 {
            return 0
        }
        return distance/time
    }
    
    public func velocityKmph(location: Location) -> Double {
        return velocityMetersPerSecond(location: location)*MpsToKmph
    }
    
    private func durationSeconds(location: Location) -> Double {
        return abs(self.timeStamp - location.timeStamp)
    }
}

extension CLGeocoder {
    public func geocodeLocation(location: Location, completion:@escaping (_ error: Error?, _ address: String) -> ()) {
        let location = CLLocation(latitude: location.latitude, longitude: location.longitude)
        
        self.reverseGeocodeLocation(location) { (placemarks, error) in
            if error == nil {
                if let placemarks = placemarks, let placemark = placemarks.first {
                    if let street = placemark.thoroughfare,
                       let city = placemark.locality,
                       let country = placemark.country {
                        completion(nil, street + ", " + city + ", " + country)
                    }
                }
            }
        }
    }
}
