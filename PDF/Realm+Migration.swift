//
//  Realm+Migration.swift
//  PDF
//
//  Created by Marcin Obolewicz on 11.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import Foundation
import RealmSwift

extension Realm {
    public static let dbVersion:UInt64 = 4
    //    Migration
    public class func performMigrations() {
        Realm.Configuration.defaultConfiguration = Realm.Configuration(
            // Set the new schema version. This must be greater than the previously used
            // version (if you've never set a schema version before, the version is 0).
            schemaVersion: Realm.dbVersion,
            migrationBlock: { migration, oldSchemaVersion in
                if oldSchemaVersion < 1 {
                    migration.enumerateObjects(ofType: Driver.className(), { (oldObject, newObject) in
                        newObject?["recentVehicleId"] = 0
                    })
                }
                if oldSchemaVersion < 2 {
                    migration.enumerateObjects(ofType: Company.className(), { (oldObject, newObject) in
                        newObject?["zipCode"] = ""
                        newObject?["city"] = ""
                        newObject?["street"] = ""
                    })
                }
                if oldSchemaVersion < 3 {
                    migration.enumerateObjects(ofType: Driver.className(), { (oldObject, newObject) in
                        newObject?["recentVehicleNumber"] = ""
                    })
                }
                if oldSchemaVersion < 4 {
                    migration.enumerateObjects(ofType: Ride.className(), { (oldObject, newObject) in
                        newObject?["vehicleRegistrationNumber"] = ""
                    })
                }
                if oldSchemaVersion == Realm.dbVersion {
                    print("No need to migrate Realm DB")
                }
        })
        let _ = try! Realm() // Invoke migration block if needed
    }
}
