//
//  RideTimelineCell.swift
//  PDF
//
//  Created by Marcin Obolewicz on 16.02.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import UIKit

class RideTimelineCell: UITableViewCell {
    @IBOutlet weak var rideLabel: UILabel!
    @IBOutlet weak var dotView: UIView!
    @IBOutlet weak var pathView: UIView!
    var ride: HistoryObjectProtocol = Ride()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupCell(ride: self.ride)
    }
    
    func setupCell(ride: HistoryObjectProtocol) {
        self.ride = ride
        self.rideLabel.text = ride.details
        let dayColor = UIColor.colorForWeekday(date: ride.date)
        self.dotView.backgroundColor =  dayColor
        self.pathView.backgroundColor =  dayColor.withAlphaComponent(0.4)
    }
}
