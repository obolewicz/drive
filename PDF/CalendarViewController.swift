//
//  CalendarViewController.swift
//  DriveMate
//
//  Created by Marcin Obolewicz on 05.01.2017.
//  Copyright © 2017 Luxmed. All rights reserved.
//

import UIKit
import JTAppleCalendar
import Popover

class CalendarViewController: UIViewController {
    @IBOutlet weak var calendarView: JTAppleCalendarView!
    @IBOutlet weak var headerView: CalendarHeaderView!
    var viewModel: HistoryViewModelProtocol!
    
    var popover: Popover!
    var popoverOptionsUp: [PopoverOption] = [
        .type(.up),
        .blackOverlayColor(UIColor(white: 0.0, alpha: 0.2))
    ]
    var popoverOptionsDown: [PopoverOption] = [
        .type(.down),
        .blackOverlayColor(UIColor(white: 0.0, alpha: 0.2))
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.calendarView.dataSource = self
        self.calendarView.delegate = self
        self.calendarView.registerCellViewXib(file: CalendarCell.identifier())
        self.setupCalendar()
        self.bindData()
    }
    
    private func bindData() {
        self.viewModel.selectedMonthString.signal.observeValues { (value: String?) in
            self.calendarView.scrollToDate(self.viewModel.selectedMonth)
        }
    }
    
    func setupCalendar() {
        self.calendarView.direction = .vertical
        self.calendarView.scrollToDate(Date())
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.view.setNeedsLayout()
    }
}

// MARK : JTAppleCalendarDelegate
extension CalendarViewController: JTAppleCalendarViewDelegate, JTAppleCalendarViewDataSource {
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy MM dd"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = Calendar.current.locale
        let startDate = formatter.date(from: "2014 12 01")!
        let endDate = formatter.date(from: "2026 10 01")!
        
        let parameters = ConfigurationParameters(startDate: startDate,
                                                 endDate: endDate,
                                                 numberOfRows: 6,
                                                 calendar: Calendar.current,
                                                 generateInDates: .forAllMonths,
                                                 generateOutDates: .tillEndOfGrid,
                                                 firstDayOfWeek: .monday)
        
        return parameters
    }
    
    func calendar(_ calendar: JTAppleCalendarView, willDisplayCell cell: JTAppleDayCellView, date: Date, cellState: CellState) {
        if let calendarCell = cell as? CalendarCell {
            
            // Setup Cell text
            calendarCell.dayLabel.text = cellState.text
            
            // Setup text color
            if cellState.dateBelongsTo == .thisMonth {
                calendarCell.dayLabel.textColor = UIColor.black
            } else {
                calendarCell.dayLabel.textColor = UIColor.gray
            }
            calendarCell.update(date: date, viewModel: self.viewModel)
        }
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleDayCellView?, cellState: CellState) {
        if let calendarCell = cell as? CalendarCell {
            if calendarCell.numberOfItems > 0 {
                self.viewModel.selectedDayItems = self.viewModel.itemsInDay(date)
                self.showPopoverTable(dayView: cell as! CalendarCell)
            }
        }
    }
    
    private func showPopoverTable(dayView: CalendarCell) {
        let maxPopoverHeight = Int(self.view.frame.height/2)-20
        let popoverHeight = 44*self.viewModel.selectedDayItems.count > maxPopoverHeight ? CGFloat(maxPopoverHeight) : CGFloat(44*self.viewModel.selectedDayItems.count)
        let tableView = UITableView(frame: CGRect(x: 0, y: 0, width: 250, height: popoverHeight))
        tableView.register(UINib(nibName:PopoverTableViewCell.identifier(), bundle:nil), forCellReuseIdentifier: PopoverTableViewCell.identifier())
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isScrollEnabled = true
        let point = dayView.superview!.convert(dayView.center, to: self.view)
        let popoverOptions = point.y < self.view.frame.height/2 ? self.popoverOptionsDown : self.popoverOptionsUp
        self.popover = Popover(options: popoverOptions, showHandler: nil, dismissHandler: nil)
        self.popover.show(tableView, point: point, inView: self.view)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        guard let date = visibleDates.monthDates.first else {
            return
        }
        self.viewModel.selectedMonth = date
        self.calendarView.reloadData()
    }
}

extension CalendarViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.popover.dismiss()
        self.viewModel.summaryAction.consume(self.viewModel.selectedDayItems[indexPath.row])
    }
}

extension CalendarViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.viewModel.selectedDayItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: PopoverTableViewCell.identifier()) as! PopoverTableViewCell
        cell.updateData(item: self.viewModel.selectedDayItems[indexPath.row])
        return cell
    }
}

