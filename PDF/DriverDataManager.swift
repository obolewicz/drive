//
//  DriverDataManager.swift
//  PDF
//
//  Created by Marcin Obolewicz on 18.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import Foundation
import RealmSwift

let DriverIdKey = "DriverIdKey"
let CompanyIdKey = "CompanyIdKey"

public class DriverDataManager: NSObject {
    static let shared = DriverDataManager()
    private override init() {
        super.init()
    }
    var currentCompany = Company(autoincrementId: true)
    var currentDriver = Driver(autoincrementId: true)
    dynamic var currentVehicle: Vehicle?
    
    public func setupDriverData() {
        self.getDriverFromRealm()
        self.getCompanyFromRealm()
        self.getVehicleFromRealm()
    }
    
    private func getDriverFromRealm() {
        let realm = try! Realm()
        let idFilter = NSPredicate(format:"driverId == %d", getCurrentDriverId())
        if let currentDriver = realm.objects(Driver.self)
            .filter(idFilter)
            .first {
            self.currentDriver = currentDriver
            
        }
        else {
            self.createNew()
        }
    }
    
    private func getCompanyFromRealm() {
        let realm = try! Realm()
        let idFilter = NSPredicate(format:"companyId == %d", self.currentDriver.companyId)
        if let currentCompany = realm.objects(Company.self)
            .filter(idFilter)
            .first {
            self.currentCompany = currentCompany
        }
    }
    
    private func getVehicleFromRealm() {
        let realm = try! Realm()
        let idFilter = NSPredicate(format:"vehicleId == %d", self.currentDriver.recentVehicleId)
        self.currentVehicle = realm.objects(Vehicle.self)
            .filter(idFilter)
            .first
    }
    
    private func getCurrentDriverId() -> Int {
        if let idString = self.getKeychainDriverId() {
            if let driverId = Int(idString) {
                return driverId
            }
        }
        else {
            self.createNew()
        }
        return 0
    }
    
    private func updateKeychainDriverId() {
        let _ = KeychainWrapper.setString(value: String(self.currentDriver.driverId), forKey: DriverIdKey)
    }
    
    private func getKeychainDriverId() -> String? {
        return KeychainWrapper.stringForKey(keyName: DriverIdKey)
    }
    
    private func createNew() {
        let realm = try! Realm()
        try! realm.write {
            realm.add(self.currentCompany)
            self.currentDriver.companyId = self.currentCompany.companyId
            currentCompany.drivers.append(self.currentDriver)
        }
        self.updateKeychainDriverId()
    }
}
