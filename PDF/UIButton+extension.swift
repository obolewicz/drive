//
//  UIButton+extension.swift
//  PDF
//
//  Created by Marcin Obolewicz on 11.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import UIKit


extension UIButton {
    public func addButtonImage(name: String, size: CGSize, insets: UIEdgeInsets) {
        if let image = UIImage(named: name) {
            let resizedImage = image.imageResize(sizeChange: size)
            self.setImage(resizedImage, for: .normal)
            self.setImage(resizedImage, for: .disabled)
            self.imageEdgeInsets = insets
        }
    }
}
