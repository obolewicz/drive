//
//  CostRate.swift
//  PDF
//
//  Created by Marcin Obolewicz on 16.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import Foundation
import RealmSwift

class CostRate: Object {
    public dynamic var kilometerCostRate: Double = 0.0
    public dynamic var beginDate: Date = Date()
    public dynamic var endDate: Date = Date()
}

