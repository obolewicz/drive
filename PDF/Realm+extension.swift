//
//  Realm+extension.swift
//  PDF
//
//  Created by Marcin Obolewicz on 20.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import RealmSwift

extension Realm {
    public class func update(completion: () -> ()) {
        let realm = try! Realm()
        try! realm.write {
            completion()
        }
    }
}

extension Object {
    public func incrementID<T: Object>(idProperty: String, class: T.Type) -> Int {
        let realm = try! Realm()
        return (realm.objects(T.self).max(ofProperty: idProperty) as Int? ?? 0) + 1
    }
}
