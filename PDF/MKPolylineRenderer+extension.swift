//
//  MKPolylineRenderer+extension.swift
//  DriveMate
//
//  Created by Marcin Obolewicz on 12.01.2017.
//  Copyright © 2017 Luxmed. All rights reserved.
//

import UIKit
import MapKit

extension MKPolylineRenderer {
    class public func customOverlayRenderer(_ overlay: MKOverlay) -> MKOverlayRenderer {
        if (overlay is MKPolyline) {
            let pr = MKPolylineRenderer(overlay: overlay)
            pr.strokeColor = UIColor.red
            pr.lineWidth = 5
            return pr
        }
        return MKOverlayRenderer()
    }
}
