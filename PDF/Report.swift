//
//  Report.swift
//  PDF
//
//  Created by Marcin Obolewicz on 04.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import Foundation
import RealmSwift

class Report: Object {
    public dynamic var reportId: Int = 0
    public dynamic var companyId: Int = 0
    public dynamic var reportBeginDate: Date = Date()
    public dynamic var reportEndDate: Date = Date()
    
    public var rides = List<Ride>()
    
    override class func primaryKey() -> String? {
        return "reportId"
    }
    
    convenience public required init(companyId: Int) {
        self.init()
        self.reportId = incrementID()
        self.companyId = companyId
    }
    
    convenience public required init(companyId: Int, beginDate: Date, endDate: Date, rides: [Ride]) {
        self.init()
        self.companyId = companyId
        self.reportId = incrementID()
        self.reportBeginDate = beginDate
        self.reportEndDate = endDate
        for ride in rides {
            self.rides.append(ride)
        }
    }
    
    private func incrementID() -> Int {
        return self.incrementID(idProperty: "reportId", class: Report.self)
    }
    
    public func calculateSumPrice(kilometerPrice: Double) -> Double {
        var sum = 0.0
        for ride in self.rides {
            sum += ride.distance/1000 * kilometerPrice
        }
        return sum
    }
    
    public func calculateSumDistance() -> Double {
        var sum = 0.0
        for ride in self.rides {
            sum += ride.distance
        }
        return sum
    }
}
