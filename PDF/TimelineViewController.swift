//
//  ListTableViewController.swift
//  DriveMate
//
//  Created by Marcin Obolewicz on 10.01.2017.
//  Copyright © 2017 Luxmed. All rights reserved.
//

import UIKit

class TimelineViewController: UITableViewController {
    var viewModel: HistoryViewModelProtocol!
    var selectedMonth: Date = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.bindData()
    }
    
    private func bindData() {
        self.viewModel.selectedMonthString.signal.observeValues { (value: String?) in
            self.tableView.reloadData()
        }
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.items.count + self.viewModel.daysWithRidesCount()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let content = self.viewModel.cellContent(index: indexPath.row)
        if let ride = content.1 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "RideTimelineCell", for: indexPath) as? RideTimelineCell {
                cell.setupCell(ride: ride)
                return cell
            }
        }
        else if let date = content.0 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "SectionTimelineCell", for: indexPath) as? SectionTimelineCell {
                cell.setupCell(date: date)
                return cell
            }
        }
        
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let rideCell = tableView.cellForRow(at: indexPath) as? RideTimelineCell {
            self.viewModel.summaryAction.consume(rideCell.ride)
        }
    }
}
