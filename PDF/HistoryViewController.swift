//
//  HistoryViewController.swift
//  DriveMate
//
//  Created by Marcin Obolewicz on 10.01.2017.
//  Copyright © 2017 Luxmed. All rights reserved.
//

import UIKit
import ReactiveCocoa
import ReactiveSwift

class HistoryViewController: UIViewController {
    @IBOutlet weak var selectedMonthLabel: UILabel!
    
    var viewModel: HistoryViewModelProtocol = HistoryViewModel()
    
    lazy var calendarViewController:CalendarViewController = {
        let viewController = UIStoryboard.viewController(storyboardName: "History", viewControllerId: "CalendarViewController") as! CalendarViewController
        viewController.viewModel = self.viewModel
        return viewController
    }()
    
    lazy var listTableViewController: PageListViewController = {
        let viewController = UIStoryboard.viewController(storyboardName: "History", viewControllerId: "PageListViewController") as! PageListViewController
        viewController.viewModel = self.viewModel
        return viewController
    }()
    @IBOutlet weak var calendarButton: UIButton!
    @IBOutlet weak var listButton: UIButton!
    
    @IBOutlet weak var journalContentView: UIView!
    
    @IBAction func calendarTabButtonTapped(_ sender: Any) {
        self.listTableViewController.hideViewController()
        self.selectedCalendarTab()
        self.showViewControllerInContainer(viewController: self.calendarViewController)
    }

    @IBAction func listTabButtonTapped(_ sender: Any) {
        self.calendarViewController.hideViewController()
        self.selectedListTab()
        self.listTableViewController.setupPageViewController()
        self.showViewControllerInContainer(viewController: self.listTableViewController)
    }
    
    var firstAppearance = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "History".localized()
        self.selectedCalendarTab()
        self.bindData()
        self.viewModel.selectedMonth = Date()
        self.createReportButton()
    }
    
    func createReportButton() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Report", style: .plain, target: self, action: #selector(showReportPreparation))
    }
    
    func showReportPreparation() {
        self.performSegue(withIdentifier: "showRaportPreparation", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if segue.identifier == "showRaportPreparation" {
            if let viewController = segue.destination as? ReportDetailsViewController {
                viewController.viewModel = ReportDetailsViewModel(date: self.viewModel.selectedMonth)
            }
        }
    }
    
    func bindData() {
        self.selectedMonthLabel.reactive.text <~ self.viewModel.selectedMonthString
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.firstAppearance {
            self.loadFirstTab()
            self.firstAppearance = false
        }
    }
    
    private func loadFirstTab() {
        self.selectedCalendarTab()
        self.showViewControllerInContainer(viewController: self.calendarViewController)
    }
    
    private func showViewControllerInContainer(viewController: UIViewController){
        self.addChildViewController(viewController)
        viewController.view.frame = self.journalContentView.frame
        self.view.addSubview(viewController.view)
    }
    
    private func selectedListTab() {
        self.listButton.backgroundColor = UIColor.customPurple()
        self.calendarButton.backgroundColor = UIColor.customGray()
    }
    
    private func selectedCalendarTab() {
        self.listButton.backgroundColor = UIColor.customGray()
        self.calendarButton.backgroundColor = UIColor.customPurple()
    }
}

