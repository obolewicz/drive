//
//  UIApplication+extension.swift
//  PDF
//
//  Created by Marcin Obolewicz on 12.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import UIKit

extension UINavigationController {
    class public func pushSummaryView(_ item: HistoryObjectProtocol) {
        if let navigation = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
            let storyboard = UIStoryboard(name: "RideSummary", bundle: Bundle.main)
            if let summaryViewController = storyboard.instantiateViewController(withIdentifier: "RideSummary") as? RideSummaryViewController {
                if let ride = item as? RideSummaryProtocol {
                    summaryViewController.viewModel.selectedItem = ride
                    navigation.pushViewController(summaryViewController, animated: true)
                }
            }
        }
    }
}
