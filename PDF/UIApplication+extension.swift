//
//  UIApplication+extension.swift
//  PDF
//
//  Created by Marcin Obolewicz on 12.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import UIKit

extension UIStoryboard {
    
    class func viewController(storyboardName: String, viewControllerId: String) -> UIViewController? {
        let storyboard = UIStoryboard(name: storyboardName, bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: viewControllerId)
    }
}

