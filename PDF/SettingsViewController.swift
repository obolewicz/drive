//
//  SettingsViewController.swift
//  PDF
//
//  Created by Marcin Obolewicz on 18.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import UIKit
import Eureka

class SettingsViewController: FormViewController {
    
    var viewModel: SettingsViewModelProtocol = SettingsViewModel()
    var selectedVehicleRow: LabelRow!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Settings".localized()
        self.setupForm()
        self.bindData()
        self.selectedVehicleRowTitle(currentVehicle: DriverDataManager.shared.currentVehicle)
    }
    
    private func bindData() {
        self.viewModel.currentVehicle.signal.observeValues { (value: Vehicle?) in
            self.selectedVehicleRowTitle(currentVehicle: value)
        }
    }
    
    private func selectedVehicleRowTitle(currentVehicle: Vehicle?) {
        if let vehicle = currentVehicle {
            self.selectedVehicleRow.value = vehicle.registrationNumber
        }
        else {
            self.selectedVehicleRow.value = "Not selected".localized()
        }
        self.selectedVehicleRow.updateCell()
    }
    
    private func setupForm() {
        self.selectedVehicleRow = setupSelectedVehicleRow()
        self.form =
            self.driverSection()
            <<< self.nameRow()
            <<< self.surnameRow()
            <<< self.emailRow()
            +++ self.vechicleSection()
            <<< self.selectedVehicleRow
            +++ self.companySection()
            <<< self.companyNameRow()
            <<< self.nipRow()
            <<< self.companyStreetRow()
            <<< self.companyCityRow()
            <<< self.zipCodeRow()
    }
    
    private func driverSection() -> Section {
        return Section("Driver Information".localized())
    }
    
    private func nameRow() -> NameRow {
        return NameRow() {
            $0.title = "Name".localized()
            $0.placeholder = "Your name".localized()
            $0.value = self.viewModel.driverName
            }.onChange({ row in
                if let value = row.value {
                    self.viewModel.driverName = value
                }
            })
    }
    
    private func surnameRow() -> NameRow {
        return NameRow() {
            $0.title = "Surname".localized()
            $0.placeholder = "Your surname".localized()
            $0.value = self.viewModel.driverSurname
            }.onChange({ row in
                if let value = row.value {
                    self.viewModel.driverSurname = value
                }
            })
    }
    
    private func emailRow() -> EmailRow {
        return EmailRow() {
            $0.title = "Email".localized()
            $0.placeholder = "your@email.com".localized()
            $0.value = self.viewModel.driverEmail
            }.onChange({ row in
                if let value = row.value {
                    self.viewModel.driverEmail = value
                }
            })
    }
    
    private func vechicleSection() -> Section {
        return Section("Vehicle Information".localized())
    }
    
    private func setupSelectedVehicleRow() -> LabelRow {
        return LabelRow () {
            $0.title = "Selected car"
            }.onCellSelection { cell, row in
                self.performSegue(withIdentifier: "selectVehicle", sender: self)
                row.reload() // or row.updateCell()
        }
    }
    
    private func companySection() -> Section {
        return Section("Company Information".localized())
    }
    
    private func companyNameRow() -> NameRow {
        return NameRow() {
            $0.title = "Name".localized()
            $0.placeholder = "Company name".localized()
            $0.value = self.viewModel.companyName
            }.onChange({ row in
                if let value = row.value {
                    self.viewModel.companyName = value
                }
            })
    }
    
    private func nipRow() -> ZipCodeRow {
        return ZipCodeRow() {
            $0.title = "NIP".localized()
            $0.placeholder = "1234567890"
            $0.value = self.viewModel.companyNIP
            }.onChange({ row in
                if let value = row.value {
                    self.viewModel.companyNIP = value
                }
            })
    }
    
    private func companyStreetRow() -> NameRow {
        return NameRow() {
            $0.title = "Street".localized()
            $0.placeholder = "Gardenstreet 1/2"
            $0.value = self.viewModel.companyStreet
            }.onChange({ row in
                if let value = row.value {
                    self.viewModel.companyStreet = value
                }
            })
    }
    
    private func companyCityRow() -> NameRow {
        return NameRow() {
            $0.title = "City".localized()
            $0.placeholder = "City name".localized()
            $0.value = self.viewModel.companyCity
            }.onChange({ row in
                if let value = row.value {
                    self.viewModel.companyCity = value
                }
            })
    }
    
    private func zipCodeRow() -> ZipCodeRow {
        return ZipCodeRow() {
            $0.title = "Zip code".localized()
            $0.placeholder = "01-001"
            $0.value = self.viewModel.companyZipCode
        }.onChange({ row in
            if let value = row.value {
                self.viewModel.companyZipCode = value
            }
        })
    }
}
