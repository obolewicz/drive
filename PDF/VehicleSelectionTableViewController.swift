//
//  VehicleSelectionTableViewController.swift
//  PDF
//
//  Created by Marcin Obolewicz on 19.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import UIKit

class VehicleSelectionTableViewController: UITableViewController {

    var viewModel: VehiclesViewModelProtocol = VehiclesViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.backgroundColor = UIColor.groupTableViewBackground
        self.tableView.tableFooterView = UIView()
        self.selectVehicleButton()
        self.title = "Vehicles List".localized()
        self.bindData()
    }
    
    func bindData() {
        self.viewModel.vehicles.signal.observeValues { (value: [Vehicle]?) in
            self.tableView.reloadData()
        }
    }
    
    func selectVehicleButton() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add".localized(), style: .plain, target: self, action: #selector(showAddVehicle))
    }
    
    func showAddVehicle() {
        let viewController = AddVehicleViewController()
        viewController.viewModel = self.viewModel
        self.navigationController?.pushViewController(viewController, animated: true)
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.vehicles.value.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "vehicleCellIdentifier", for: indexPath) as? VehicleTableViewCell {
            cell.update(vehicle: self.viewModel.vehicles.value[indexPath.row])
            return cell
        }
        return UITableViewCell()
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "VEHICLES".localized()
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.viewModel.deleteVehicle(at: indexPath.row)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewModel.selectVehicle(at: indexPath.row)
        let _ = self.navigationController?.popViewController(animated: true)
    }

}
