//
//  NSDate+extension.swift
//  PDF
//
//  Created by Marcin Obolewicz on 11.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import Foundation

public func ==(lhs: Date, rhs: Date) -> Bool {
    return lhs.compare(rhs) == .orderedSame
}

public func <(lhs: Date, rhs: Date) -> Bool {
    return lhs.compare(rhs) == .orderedAscending
}

extension Date {
    func sameDay(date: Date) -> Bool {
        let calendar: Calendar = Calendar.current
        
        let componentsForFirstDate: DateComponents = calendar.dateComponents([.day, .month, .year], from: self)
        let componentsForSecondDate: DateComponents = calendar.dateComponents([.day, .month, .year], from: date)
        if componentsForFirstDate.day == componentsForSecondDate.day
            && componentsForFirstDate.month == componentsForSecondDate.month
            && componentsForFirstDate.year == componentsForSecondDate.year {
            return true
        }
        return false
        
    }
    
    public func previousMonth() -> Date? {
        let calendar = Calendar.current
        
        return calendar.date(byAdding: .month, value: -1, to: self)
    }
    
    public func nextMonth() -> Date? {
        let calendar = Calendar.current
        return calendar.date(byAdding: .month, value: 1, to: self)
    }
    
    public func previousDay() -> Date? {
        let calendar = Calendar.current
        return calendar.date(byAdding: .day, value: -1, to: self)
    }
    
    public func nextDay() -> Date {
        let calendar = Calendar.current
        return calendar.date(byAdding: .day, value: 1, to: self)!
    }
    
    public func isLessThanDate(dateToCompare: Date) -> Bool {
        var isLess = false
        if self.compare(dateToCompare) == ComparisonResult.orderedAscending {
            isLess = true
        }
        return isLess
    }
    
    public func addTime(unit:Int, unitType:Calendar.Component) -> Date{
        var components: DateComponents = DateComponents()
        components.setValue(unit, for: unitType)
        
        return Calendar.current.date(byAdding: components, to: self)!
    }
    
    public func addHours(hoursToAdd: Int) -> Date {
        return self.addTime(unit: hoursToAdd, unitType:Calendar.Component.hour)
    }
    
    public func addMinutes(minutesToAdd: Int) -> Date {
        return self.addTime(unit: minutesToAdd, unitType:Calendar.Component.minute)
    }
    
    public func monthBeginDate() -> Date {
        let calendar:Calendar = Calendar.current
        var components:DateComponents = calendar.dateComponents([.year, .month, .day], from: self)
        components.setValue(1, for: .day)
        if let firstDayOfMonthDate = calendar.date(from: components) {
            return firstDayOfMonthDate
        }
        return Date()
    }
    
    public func monthEndDate() -> Date {
        let calendar:Calendar = Calendar.current
        var components:DateComponents = calendar.dateComponents([.year, .month, .day], from: self)
        components.setValue(1, for: .day)
        if let firstDayOfMonthDate = calendar.date(from: components) {
            if let firstDayOfNextMonthDate = calendar.date(byAdding: .month, value: 1, to: firstDayOfMonthDate) {
                return firstDayOfNextMonthDate
            }
        }
        return Date()
    }
    
    public func dayBeginDate() -> Date {
        let calendar:Calendar = Calendar.current
        var components:DateComponents = calendar.dateComponents([.year, .month, .day, .hour], from: self)
        components.setValue(0, for: .hour)
        if let firstDayOfMonthDate = calendar.date(from: components) {
            return firstDayOfMonthDate
        }
        return Date()
    }
    
    public func dayEndDate() -> Date {
        let calendar:Calendar = Calendar.current
        var components:DateComponents = calendar.dateComponents([.year, .month, .day, .hour], from: self)
        components.setValue(0, for: .hour)
        if let beginOfDayDate = calendar.date(from: components) {
            if let endOfDayDate = calendar.date(byAdding: .day, value: 1, to: beginOfDayDate) {
                return endOfDayDate
            }
        }
        return Date()
    }
    
    public func isSameDay(day: Date) -> Bool {
        let calendar:Calendar = Calendar.current
        let components:DateComponents = calendar.dateComponents([.year, .month, .day], from: self)
        let componentsDay:DateComponents = calendar.dateComponents([.year, .month, .day], from: day)
        if componentsDay.year == components.year
            && componentsDay.month == components.month
            && componentsDay.day == components.day {
            return true
        }
        return false
    }
}
