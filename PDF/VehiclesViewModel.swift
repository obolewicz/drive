//
//  VehiclesViewModel.swift
//  PDF
//
//  Created by Marcin Obolewicz on 19.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import Foundation
import RealmSwift
import ReactiveSwift

protocol VehiclesViewModelProtocol {
    var vehicles: MutableProperty<[Vehicle]>{ get set }
    var newVehicleRegistrationNumber: String { get set }
    var newVehicleMilage: Double { get set }
    var newVehicleEngineDisplacement: Double { get set }
    func addVehicle()
    func updateVehicles()
    func selectVehicle(at index: Int)
    func deleteVehicle(at index: Int)
}

class VehiclesViewModel: VehiclesViewModelProtocol {
    dynamic public var newVehicleRegistrationNumber: String = ""
    dynamic public var newVehicleMilage: Double = 20000
    dynamic public var newVehicleEngineDisplacement: Double = 2.0
    var vehicles = MutableProperty([Vehicle]())
    
    init() {
        self.updateVehicles()
    }
    
    func updateVehicles() {
        self.vehicles.value = self.getCompanyVehicles(companyId: DriverDataManager.shared.currentCompany.companyId)
    }
    
    func selectVehicle(at index: Int) {
        Realm.update {
            DriverDataManager.shared.currentDriver.recentVehicleId = self.vehicles.value[index].vehicleId
        }
        DriverDataManager.shared.currentVehicle = self.vehicles.value[index]
    }
    
    func addVehicle() {
        let companyId = DriverDataManager.shared.currentDriver.companyId
        let newVehicle = Vehicle(registrationNumber: newVehicleRegistrationNumber, milage: newVehicleMilage, engine: newVehicleEngineDisplacement, companyId: companyId)
        let realm = try! Realm()
        try! realm.write {
            DriverDataManager.shared.currentCompany.vehicles.append(newVehicle)
        }
        self.updateVehicles()
    }
    
    func deleteVehicle(at index: Int) {
        let deleteVehicle = self.vehicles.value[index]
        if let vehicle = DriverDataManager.shared.currentVehicle {
            if vehicle.registrationNumber == deleteVehicle.registrationNumber {
                DriverDataManager.shared.currentVehicle = nil
                Realm.update {
                    DriverDataManager.shared.currentDriver.recentVehicleId = -1
                }
            }
        }
        let realm = try! Realm()
        try! realm.write {
            realm.delete(deleteVehicle)
        }
        self.vehicles.value.remove(at: index)
    }
    
    private func getCompanyVehicles(companyId: Int) -> [Vehicle] {
        return Array(DriverDataManager.shared.currentCompany.vehicles)
    }
    
}
