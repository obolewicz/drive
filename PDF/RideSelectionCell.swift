//
//  RideSelectionCell.swift
//  PDF
//
//  Created by Marcin Obolewicz on 16.02.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import UIKit
import Eureka

internal final class ListCheckRow: Row<RideSelectionCell>, RowType {
    public var ride: Ride? {
        didSet {
            if let ride = self.ride {
                self.cell.setupLabels(ride: ride)
            }
        }
    }
    
    required public init(tag: String?) {
        super.init(tag: tag)
        displayValueFor = nil
        cellProvider = CellProvider<RideSelectionCell>(nibName: "RideSelectionCell")
        self.baseCell.height = { return CGFloat(90.0)}
    }
    
    override func didSelect() {
        super.didSelect()
        if !self.isDisabled {
            self.value = !self.value!
        }
        self.deselect()
        self.updateCell()
    }
}


class RideSelectionCell: Cell<Bool>, CellType{
    @IBOutlet weak var beginRideDateLabel: UILabel!
    @IBOutlet weak var beginLocationLabel: UILabel!
    @IBOutlet weak var endLocationLabel: UILabel!
    @IBOutlet weak var distanceValueLabel: UILabel!
    
    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var toLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
    open override func update() {
        super.update()
        accessoryType = row.value! ? .checkmark : .none
    }
    
    public func setupLabels(ride: Ride) {
        self.beginRideDateLabel.text = DateFormatterHelper.sharedInstance.rideBeginString(date: ride.beginDate)
        self.beginLocationLabel.text = ride.startAddress
        self.endLocationLabel.text = ride.endAddress
        self.distanceValueLabel.text = ride.distance.distanceFormatted()
    }
    
    open override func setup() {
        super.setup()
        if row.value == nil {
            row.value = false
        }
        accessoryType =  .checkmark
        editingAccessoryType = accessoryType
    }
    
    open override func didSelect() {
        super.didSelect()
        row.updateCell()
    }
}
