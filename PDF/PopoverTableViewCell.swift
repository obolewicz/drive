//
//  PopoverTableView.swift
//  DriveMate
//
//  Created by Marcin Obolewicz on 09.01.2017.
//  Copyright © 2017 Luxmed. All rights reserved.
//

import UIKit

class PopoverTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    
    class public func identifier() -> String {
        return "PopoverTableViewCell"
    }
    
    public func updateData(item: HistoryObjectProtocol) {
        self.titleLabel.text = item.title
        self.detailsLabel.text = item.details
    }
}
