//
//  AddVehicleViewController.swift
//  PDF
//
//  Created by Marcin Obolewicz on 19.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import UIKit
import Eureka

class AddVehicleViewController: FormViewController {

    var viewModel: VehiclesViewModelProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        self.title = "Add New Vehicle".localized()
        self.selectVehicleButton()
        self.setupForm()
    }
    
    private func setupForm() {
        self.form =
            self.vechicleSection()
            <<< self.registrationNumberRow()
            <<< self.engineDisplacementRow()
            <<< self.milageRow()
            +++ Section()
    }

    func selectVehicleButton() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done".localized(), style: .done, target: self, action: #selector(showVehiclesList))
    }
    
    func showVehiclesList() {
        self.viewModel.addVehicle()
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    private func vechicleSection() -> Section {
        return Section("Vehicle Information".localized())
    }
    
    private func registrationNumberRow() -> NameRow {
        return NameRow() {
            $0.title = "Registration number".localized()
            $0.placeholder = "number".localized()
        }.onChange({ row in
            if let value = row.value {
                self.viewModel.newVehicleRegistrationNumber = value.uppercased()
            }
        })
        
    }
    
    private func engineDisplacementRow() -> StepperRow  {
        return StepperRow(){
            $0.title = "Engine displacement".localized()
            $0.value = 2.0
            }.cellSetup { cell, row in
                cell.stepper.maximumValue = 10.0
                cell.stepper.minimumValue = 0.1
                cell.stepper.stepValue = 0.1
            }.onChange({ row in
                if let value = row.value {
                    self.viewModel.newVehicleEngineDisplacement = value
                }
            })
    }
    
    private func milageRow() -> IntRow  {
        return IntRow() {
            $0.title = "Milage".localized()
            $0.value = 20000
            }.onChange({ row in
                if let value = row.value {
                    self.viewModel.newVehicleMilage = Double(value)
                }
            })
    }
    
}
