//
//  UIColor+custom.swift
//  PDF
//
//  Created by Marcin Obolewicz on 12.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import UIKit

extension UIColor {
    static let weekdayColors: [UIColor] = [
        UIColor(red: 0.56, green: 0.86, blue: 0.78, alpha: 1),
        UIColor(red: 0.55, green: 0.75, blue: 0.93, alpha: 1),
        UIColor(red: 0.99, green: 0.91, blue: 0.67, alpha: 1),
        UIColor(red: 0.93, green: 0.59, blue: 0.57, alpha: 1),
        UIColor(red: 0.85, green: 0.73, blue: 0.84, alpha: 1),
        UIColor(red: 0.93, green: 0.59, blue: 0.57, alpha: 1),
        UIColor(red: 0.85, green: 0.73, blue: 0.84, alpha: 1)]
    
    public class func customPurple() -> UIColor{
        return UIColor(red: 0.24, green: 0.11, blue: 0.33, alpha: 1)
    }
    public class func customFushia() -> UIColor{
        return UIColor(red: 0.66, green: 0.14, blue: 0.52, alpha: 1)
    }
    public class func customGray() -> UIColor{return UIColor(red: 0.85, green: 0.85, blue: 0.85, alpha: 1)}
    
    public class func colorForWeekday(date: Date) -> UIColor {
        let calendar:Calendar = Calendar.current
        let components:DateComponents = calendar.dateComponents([.weekday], from: date)
        if let weekday = components.weekday {
            return weekdayColors[weekday-1]
        }
        return weekdayColors[0]
    }
}
