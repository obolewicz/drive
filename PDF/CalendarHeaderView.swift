//
//  CalendarHeaderView.swift
//  DriveMate
//
//  Created by Marcin Obolewicz on 08.01.2017.
//  Copyright © 2017 Luxmed. All rights reserved.
//

import UIKit

@IBDesignable class CalendarHeaderView: UIView {
    var view: UIView!
    
    @IBOutlet weak var stackView: UIStackView!
    
    override init(frame: CGRect){
        super.init(frame: frame)
        setup()
    }
    
    required init(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)!
        setup()
    }
    
    private func setup(){
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView{
        let bundle = Bundle(for:type(of: self))
        let nib = UINib(nibName: "CalendarHeaderView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
}
