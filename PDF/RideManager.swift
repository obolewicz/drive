//
//  RideManager.swift
//  PDF
//
//  Created by Marcin Obolewicz on 12.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import Foundation
import CoreLocation
import ReactiveCocoa
import ReactiveSwift
import RealmSwift

public class RideManager: AbstractRideManager {
    static let shared = RideManager()
    internal var location: DynamicProperty<CLLocation>
    public var ride: Ride?
    private var currentLocationPart: Int = 0
    
    var locationManager = LocationManager()
    
    private override init() {
        self.location = DynamicProperty<CLLocation>(object: self.locationManager, keyPath: "currentLocation")
        super.init()
        self.bindData()
    }
    
    func bindData() {
        self.location.signal.observeValues { (value: CLLocation?) in
            if let location = value {
                self.appendLocation(location: location)
            }
        }
    }
    
    override public func start() {
        super.start()
        self.createRide()
    }
    
    public override func stop() {
        super.stop()
        self.endRide()
       
        if let ride = self.ride {
            if ride.locations.count > 2 {
                let geocoder = CLGeocoder()
                geocoder.geocodeLocation(location: ride.locations.first!, completion: {
                    (errorFirst: Error?, addressFirst: String) in
                    if errorFirst == nil {
                        geocoder.geocodeLocation(location: ride.locations.last!, completion: {
                            (errorLast: Error?, addressLast: String) in
                            if errorLast == nil {
                                Realm.update {
                                    ride.rideRouteDescription = addressFirst + " to ".localized() + addressLast
                                }
                                UINavigationController.pushSummaryView(ride)
                            }
                        })
                    }
                })
            }
            else {
                UINavigationController.pushSummaryView(ride)
            }
        }
    }
    
    private func createRide() -> Void {
        let realm = try! Realm()
        try! realm.write {
            self.ride = Ride(beginDate: self.startTime, driverId: DriverDataManager.shared.currentDriver.driverId, vehicleId: DriverDataManager.shared.currentDriver.recentVehicleId)
            if let ride = self.ride {
                realm.add(ride)
            }
        }
    }

    private func appendLocation(location: CLLocation) {
        if let ride = self.ride {
            let location = Location(location: location)
            let lastLocation = ride.locations.last
            if lastLocation != nil {
                location.distance = location.distanceMeters(location: lastLocation!)
            }
            self.distance = self.distance + location.distance
            
            let realm = try! Realm()
            try! realm.write {
                ride.locations.append(location)
                ride.distance = self.distance
            }
        }
    }
    
    private func endRide() -> Void {
        if let ride = self.ride {
            let realm = try! Realm()
            try! realm.write {
                ride.distance = self.distance
                ride.duration = Int(self.duration)
                ride.beginDate = self.startTime
                ride.endDate = self.endTime
            }
        }
    }

    public func rideRouteDescriptionUpdate(location: String) {
        self.ride?.rideRouteDescription += location
    }
}
