//
//  User.swift
//  PDF
//
//  Created by Marcin Obolewicz on 04.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import Foundation
import RealmSwift

class Driver: Object {
    public dynamic var driverId = 0
    public dynamic var recentVehicleId = 0
    public dynamic var companyId: Int = 0
    public dynamic var email: String = ""
    public dynamic var firstName: String = ""
    public dynamic var lastName: String = ""
    
    public func fullName() -> String {
        return firstName + " " + lastName
    }
    
    override class func primaryKey() -> String? {
        return "driverId"
    }
    
    convenience public required init(autoincrementId: Bool) {
        self.init()
        if autoincrementId {
            self.driverId = incrementID()
        }
    }
    
    private func incrementID() -> Int {
        return self.incrementID(idProperty: "driverId", class: Driver.self)
    }
}
