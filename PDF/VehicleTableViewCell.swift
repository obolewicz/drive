//
//  VehicleTableViewCell.swift
//  PDF
//
//  Created by Marcin Obolewicz on 20.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import UIKit

class VehicleTableViewCell: UITableViewCell {
    @IBOutlet weak var milageLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    internal func update(vehicle: Vehicle) {
        self.numberLabel.text = vehicle.registrationNumber
        self.milageLabel.text = String(vehicle.milage)
    }
}
