//
//  MKMapView+extension.swift
//  DriveMate
//
//  Created by Marcin Obolewicz on 12.01.2017.
//  Copyright © 2017 Luxmed. All rights reserved.
//

import UIKit
import MapKit

extension MKMapView {
    
    func drawRoute(ride: RideSummaryProtocol){
        self.removeAnnotations(self.annotations)
        self.drawRideRoute(ride: ride)
        self.region = mapRegion(ride: ride)
    }
    
    func mapRegion(ride: RideSummaryProtocol) -> MKCoordinateRegion {
        let firstLocation = ride.locationsDisplay.first?.coordinate()
        var minLat = firstLocation!.latitude
        var minLng = firstLocation!.longitude
        var maxLat = firstLocation!.latitude
        var maxLng = firstLocation!.longitude
        
        for locationTraining in ride.locationsDisplay {
            let location = locationTraining.coordinate()
            if location.latitude < minLat {
                minLat = location.latitude
            }
            if location.longitude < minLng {
                minLng = location.longitude
            }
            if location.latitude > maxLat {
                maxLat = location.latitude
            }
            if location.longitude > maxLng {
                maxLng = location.longitude
            }
        }
        let center = CLLocationCoordinate2D(latitude: (minLat + maxLat) / 2.0, longitude: (minLng + maxLng) / 2.0)
        // 70% padding
        let span =  MKCoordinateSpan(latitudeDelta: (maxLat - minLat) * 1.7, longitudeDelta: (maxLng - minLng) * 1.7)
        return MKCoordinateRegion(center: center, span: span)
    }
    
    private func drawRideRoute(ride: RideSummaryProtocol) {
        var coordinates:[CLLocationCoordinate2D] = []
        for locationObject in ride.locationsDisplay {
            coordinates.append(locationObject.coordinate())
        }
        let route = MKPolyline(coordinates: &coordinates, count: coordinates.count)
        self.add(route)
    }
}
