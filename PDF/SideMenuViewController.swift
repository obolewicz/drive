//
//  SideMenuViewController.swift
//  PDF
//
//  Created by Marcin Obolewicz on 21.02.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import UIKit
import SideMenuController

class SideMenuViewController: UIViewController {

    @IBAction func showRideView(_ sender: Any) {
        self.sideMenuController?.performSegue(withIdentifier: "ride", sender: nil)
    }
    
    @IBAction func showCalendarView(_ sender: Any) {
        self.sideMenuController?.performSegue(withIdentifier: "history", sender: nil)
    }
    
    @IBAction func showReportsView(_ sender: Any) {
        self.sideMenuController?.performSegue(withIdentifier: "reports", sender: nil)
    }
    
    @IBAction func showProfileView(_ sender: Any) {
            self.sideMenuController?.performSegue(withIdentifier: "settings", sender: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

}
