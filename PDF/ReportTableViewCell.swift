//
//  ReportTableViewCell.swift
//  PDF
//
//  Created by Marcin Obolewicz on 21.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import UIKit

class ReportTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var beginDateLabel: UILabel!
    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet weak var ridesNumberValueLabel: UILabel!
    @IBOutlet weak var costValueLabel: UILabel!
    
    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var toLabel: UILabel!
    @IBOutlet weak var ridesLabel: UILabel!
    @IBOutlet weak var costLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    internal func update(report: Report) {
        
        self.beginDateLabel.text = DateFormatterHelper.sharedInstance.monthYearJournalStringFromDate(date: report.reportBeginDate)
        self.endDateLabel.text = DateFormatterHelper.sharedInstance.monthYearJournalStringFromDate(date: report.reportEndDate)
        self.costValueLabel.text = report.calculateSumPrice(kilometerPrice: KilometerCostManager.currentKilometerCost()).priceFormatted()
        self.ridesNumberValueLabel.text = String(report.rides.count)
    }
    
}
