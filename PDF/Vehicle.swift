//
//  Vehicle.swift
//  PDF
//
//  Created by Marcin Obolewicz on 04.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import UIKit
import RealmSwift
import Realm

class Vehicle: Object {
    public dynamic var vehicleId: Int = 0
    public dynamic var companyId: Int = 0
    public dynamic var registrationNumber: String = ""
    public dynamic var engineDisplacement:Double = 0.0
    public dynamic var milage: Double = 0.0
 
    convenience public required init(autoincrementId: Bool) {
        self.init()
        if autoincrementId {
            self.vehicleId = incrementID()
        }
    }
    
    override class func primaryKey() -> String? {
        return "vehicleId"
    }
    
    convenience public required init(registrationNumber: String, milage: Double, engine: Double, companyId: Int) {
        self.init()
        self.vehicleId = self.incrementID()
        self.registrationNumber = registrationNumber
        self.milage = milage
        self.engineDisplacement = engine
        self.companyId = companyId
    }
    
    private func incrementID() -> Int {
        return self.incrementID(idProperty: "vehicleId", class: Vehicle.self)
    }
}
