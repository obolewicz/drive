//
//  AbstractRideManager.swift
//  PDF
//
//  Created by Marcin Obolewicz on 12.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import UIKit
import Result
import ReactiveCocoa
import ReactiveSwift

@objc public enum RideState: Int {
    case stopped
    case running
    case paused
}

open class AbstractRideManager: NSObject {
    
    dynamic public var state: RideState = .stopped
    dynamic public var duration: Double = 0.0
    dynamic public var distance: Double = 0.0
    
    open var startTime = Date()
    open var endTime = Date()
    open var timer = Timer()
    open var durationChange: Double = 0.0
    open var pausedDuration: Double = 0.0
    open var pausedTime = Date()
    
    open let (locationOutputSignal, locationObserver) = Signal<(current: Location, previous: Location?), NoError>.pipe()
    public var locationSignal: Signal<(current: Location, previous: Location?), NoError> {
        return locationOutputSignal
    }
    
    public override init() {
        super.init()
        
        self.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(AbstractRideManager.counter), userInfo: nil, repeats: true)
        RunLoop.current.add(self.timer, forMode: RunLoopMode.commonModes)
    }
    
    open func start() -> Void {
            self.cleanup()
            self.startTime = Date()
            self.state = .running
    }
    
    open func resume() -> Void {
        self.pausedDuration += NSDate().timeIntervalSince(self.pausedTime as Date)
        self.state = .running
    }
    
    open func pause() -> Void {
        self.state = .paused
        self.pausedTime = Date()
    }
    
    open func stop() -> Void {
        self.state = .stopped
        self.endTime = Date()
    }
    
    open func abort() -> Void {
        self.stop()
    }
    
    open func counter() -> Void {
        if self.state == .running{
            self.updateDuration()
        }
    }
    
    open func updateDuration() -> Void {
        self.durationChange = (NSDate().timeIntervalSince1970 - self.startTime.timeIntervalSince1970) - self.duration - self.pausedDuration
        self.duration += self.durationChange
        self.endTime = Date()
    }
    
    open func cleanup() -> Void {
        self.duration = 0.0
        self.distance = 0.0
        self.pausedDuration = 0.0
        self.endTime = Date()
    }
}
