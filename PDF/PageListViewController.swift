//
//  PageListViewController.swift
//  DriveMate
//
//  Created by Marcin Obolewicz on 10.01.2017.
//  Copyright © 2017 Luxmed. All rights reserved.
//

import UIKit

class PageListViewController: UIViewController {
    var viewModel: HistoryViewModelProtocol!
    var pageViewController: UIPageViewController!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupPageViewController()
    }
    
    func setupPageViewController() {
        self.pageViewController = UIPageViewController(transitionStyle: UIPageViewControllerTransitionStyle.scroll, navigationOrientation: UIPageViewControllerNavigationOrientation.horizontal, options: nil)
        self.pageViewController.view.frame = self.view.bounds
        self.pageViewController.dataSource = self
        self.pageViewController.delegate = self
        
        let viewControllers = [self.listTableViewController(date: self.viewModel.selectedMonth)]
        self.pageViewController.setViewControllers(viewControllers, direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
        
        self.view.addSubview(self.pageViewController.view)
    }
    
    func listTableViewController(date: Date) -> UIViewController {
        let currentMonthViewController = UIStoryboard(name: "History", bundle: nil).instantiateViewController(withIdentifier: "Timeline") as! TimelineViewController 
        currentMonthViewController.viewModel = self.viewModel
        currentMonthViewController.selectedMonth = date
        return currentMonthViewController
    }
    
    func listTableViewFromList(viewControllers: [UIViewController]) -> TimelineViewController? {
        if let viewController = viewControllers.first {
            if let pc = viewController as? TimelineViewController {
                return pc
            }
        }
        return nil
    }
    
    func refreshList() {
        if let pageViewController = self.pageViewController {
            if let listPageViewControllers = pageViewController.viewControllers {
                if let listTable = self.listTableViewFromList(viewControllers: listPageViewControllers){
                    listTable.tableView.reloadData()
                }
            }
        }
    }
}

extension PageListViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            if let previousController = self.listTableViewFromList(viewControllers: previousViewControllers) {
                if let currentController = self.listTableViewFromList(viewControllers: self.pageViewController.viewControllers!) {
                    if previousController.selectedMonth.isLessThanDate(dateToCompare: currentController.selectedMonth) {
                        self.viewModel.setNextMonth()
                    }
                    else {
                        self.viewModel.setPrevMonth()
                    }
                }
            }
        }
    }
}

extension PageListViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if let newDate = self.viewModel.selectedMonth.nextMonth() {
            return self.listTableViewController(date: newDate)
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if let newDate = self.viewModel.selectedMonth.previousMonth() {
            return self.listTableViewController(date: newDate)
        }
        return nil
    }
    
}
