//
//  LocationManager.swift
//  PDF
//
//  Created by Marcin Obolewicz on 13.01.2017.
//  Copyright © 2017 Marcin Obolewicz. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import ReactiveSwift

class LocationManager: NSObject {
    dynamic var currentLocation: CLLocation?
    var previousLocation: CLLocation?
    dynamic var polyline: MKPolyline?
    lazy var locationMgr: CLLocationManager = {
        var lm = CLLocationManager()
        lm.delegate = self
        lm.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        lm.activityType = .automotiveNavigation
        lm.allowsBackgroundLocationUpdates = true
        let status = CLLocationManager.authorizationStatus()
        if status == .notDetermined || status == .denied || status == .authorizedWhenInUse {
            lm.requestAlwaysAuthorization()
        }
        lm.startUpdatingLocation()
        lm.startUpdatingHeading()
        lm.distanceFilter = 4.0
        return lm
    }()
    
    override init() {
        super.init()
        self.locationMgr.startUpdatingHeading()
        self.locationMgr.startUpdatingLocation()
    }
    
    func resetMap(){
        self.previousLocation = nil
    }
}

extension LocationManager: CLLocationManagerDelegate{
    
    @nonobjc func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == .notDetermined {
            manager.requestWhenInUseAuthorization()
        }
        
    }
    
    func acceptableHorizontalAccuracyForDevice() -> Double {
        if UIDevice.isiPad() {
            return 80.0
        }
        return 20.0
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let newLocation = locations.first {
            if RideManager.shared.state == .running
                && newLocation.horizontalAccuracy < self.acceptableHorizontalAccuracyForDevice() {
                self.previousLocation = self.currentLocation
                self.currentLocation = newLocation
                self.calculateNewPolyline()
            }
        }
    }
    
    private func calculateNewPolyline(){
        if let previousLocation = self.previousLocation,
            let currentLocation = self.currentLocation {
            
            let previousCoordinates = previousLocation.coordinate
            let currentCoordinates = currentLocation.coordinate
            var area = [previousCoordinates, currentCoordinates]
            
            self.polyline =  MKPolyline(coordinates: &area, count: area.count)
        }
    }
}
